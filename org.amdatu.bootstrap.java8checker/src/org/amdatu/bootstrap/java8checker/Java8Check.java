/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.java8checker;

import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.Inject;
import org.apache.felix.dm.annotation.api.Start;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;

@Component
public class Java8Check {

	@Inject 
	private volatile BundleContext m_bundleContext;
	
	
	@Start
	public void start() throws BundleException {
		String vmVersionString = System.getProperty("java.vm.specification.version");
		double vmVersion = Double.parseDouble(vmVersionString);
		if(vmVersion < 1.8) {
			System.err.println("Amdatu Bootstrap requires Java 8, but was started on JRE " + vmVersion);
			System.exit(0);
		}
	}
}
