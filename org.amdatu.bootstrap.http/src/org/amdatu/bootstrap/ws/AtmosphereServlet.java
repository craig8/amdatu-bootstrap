/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.ws;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.felix.dm.DependencyManager;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.Inject;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.apache.felix.dm.annotation.api.Start;
import org.apache.felix.dm.annotation.api.Stop;
import org.atmosphere.cpr.AtmosphereFramework;
import org.atmosphere.cpr.AtmosphereInterceptor;
import org.atmosphere.cpr.AtmosphereRequest;
import org.atmosphere.cpr.AtmosphereResponse;
import org.atmosphere.handler.OnMessage;
import org.atmosphere.interceptor.AtmosphereResourceLifecycleInterceptor;
import org.atmosphere.interceptor.BroadcastOnPostAtmosphereInterceptor;
import org.osgi.service.http.HttpService;
import org.osgi.service.http.NamespaceException;

@Component
public class AtmosphereServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private final AtmosphereFramework framework;

	@ServiceDependency
	private volatile HttpService httpService;
	
	@ServiceDependency 
	private volatile OnMessage<String> m_messageHandler;
	
	@Inject
	private volatile DependencyManager m_dm;

	public AtmosphereServlet() {
		framework = new AtmosphereFramework(false, false);
	}

	@Start
	public void start() throws ServletException, NamespaceException {
		ClassLoader loader = Thread.currentThread().getContextClassLoader();
		Thread.currentThread().setContextClassLoader(AtmosphereFramework.class.getClassLoader());

		try {
			Hashtable<String, Object> properties = new Hashtable<String, Object>();
			httpService.registerServlet("/atmosphere", this, properties, null);
		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			Thread.currentThread().setContextClassLoader(loader);
		}
	}

	@Stop
	public void stop() {
		framework.removeAtmosphereHandler("/atmosphere/bootstrap");
		httpService.unregister("/atmosphere");
	}

	@Override
	public void init(ServletConfig config) throws ServletException {

		framework.init(config);
		
		List<AtmosphereInterceptor> interceptors = new ArrayList<>();
		interceptors.add(new BroadcastOnPostAtmosphereInterceptor());
		interceptors.add(new AtmosphereResourceLifecycleInterceptor());
		framework.addAtmosphereHandler("/atmosphere/bootstrap", m_messageHandler, interceptors);
	}

	@Override
	protected void doDelete(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doPost(req, resp);
	}

	@Override
	protected void doHead(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doPost(req, resp);
	}

	@Override
	protected void doOptions(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		framework.doCometSupport(AtmosphereRequest.wrap(req), AtmosphereResponse.wrap(resp));
	}

	@Override
	protected void doPut(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doPost(req, resp);
	}

}
