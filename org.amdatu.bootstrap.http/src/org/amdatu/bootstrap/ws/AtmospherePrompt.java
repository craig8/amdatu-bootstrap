/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.ws;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

import org.amdatu.bootstrap.command.FullyQualifiedName;
import org.amdatu.bootstrap.services.Prompt;
import org.atmosphere.cpr.AtmosphereResource;

import rx.observables.BlockingObservable;

public class AtmospherePrompt implements Prompt {

	private AtmosphereResource m_resource;
	private BlockingObservable<Object> m_observable;
	
	public AtmospherePrompt(AtmosphereResource resource, BlockingObservable<Object> observable) {
		this.m_resource = resource;
		this.m_observable = observable;
	}
	
	@Override
	public boolean askBoolean(String message, boolean defaultChoice) {
		String stringValue = createPromptQuestion(message, String.valueOf(defaultChoice), Boolean.class);
		return Boolean.parseBoolean(stringValue);
	}

	@Override
	public String askString(String message) {
		return createPromptQuestion(message, null, String.class);
	}

	@Override
	public String askString(String message, String defaultString) {
		return createPromptQuestion(message, defaultString, getClass());
	}

	@Override
	public <T> T askChoice(String message, int defaultOption, List<T> options) {
		int selectedIndex = createChoicePromptQuestion(message, defaultOption, options);
		return options.get(selectedIndex);
	}

	@Override
	public int askChoiceAsIndex(String message, int defaultOption,
			List<? extends Object> options) {
		return createChoicePromptQuestion(message, defaultOption, options);
	}

	private int createChoicePromptQuestion(String message, int defaultOption, List<? extends Object> options) {
		PromptQuestion q = new PromptQuestion();
		q.setId(UUID.randomUUID().toString());
		q.setType(Integer.class.getName());
		q.setDescription(message);
		q.setDefaultOption(defaultOption);
		q.setOptions(options.stream().map(o -> o.toString()).collect(Collectors.toList()));

		sendPromptQuestion(q);
		
		Iterable<Object> next = m_observable.next();
		
		String choiceStr = (String)next.iterator().next();
		return Integer.parseInt(choiceStr);
	}

	private void sendPromptQuestion(PromptQuestion q) {
		m_resource.write(new BootstrapMessageFactory().createAsBytes("prompt", q));
	}

	@Override
	public FullyQualifiedName askComponentName() {
		String answer = createPromptQuestion("What is the name of the component", null, FullyQualifiedName.class);
		
		return new FullyQualifiedName(answer);
	}

	@Override
	public FullyQualifiedName askComponentName(String message) {
		String answer = createPromptQuestion(message, null, FullyQualifiedName.class);
		return new FullyQualifiedName(answer);
	}

	@Override
	public FullyQualifiedName askComponentName(String message, String defaultName) {
		String answer = createPromptQuestion("What is the name of the component", defaultName, FullyQualifiedName.class);
		return new FullyQualifiedName(answer);
	}
	
	private String createPromptQuestion(String message, String defaultValue, Class<?> type) {
		PromptQuestion q = new PromptQuestion();
		q.setId(UUID.randomUUID().toString());
		q.setType(type.getName());
		q.setDescription(message);
		q.setDefaultValue(defaultValue);

		sendPromptQuestion(q);
		
		Iterable<Object> next = m_observable.next();
		return (String) next.iterator().next();
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public Map<String, Boolean> askMultipleBoolean(String message, Map<String, String> questions, boolean defaultChoice) {
		PromptMultiQuestion<Boolean> q = new PromptMultiQuestion<>();
		q.setId(UUID.randomUUID().toString());
		q.setType(Map.class.getName());
		q.setDescription(message);
		q.setDefaultOption(defaultChoice);
		q.setQuestions(questions);

		m_resource.write(new BootstrapMessageFactory().createAsBytes("prompt-multi", q));
		
		Iterable<Object> next = m_observable.next();
		
		return (Map)next.iterator().next();
	}

	@Override
	public Map<String, Boolean> askMultipleBoolean(String message, List<String> questions, boolean defaultChoice) {
		Map<String, String> map = new HashMap<>();
		questions.forEach(s -> map.put(s, null));
		
		return askMultipleBoolean(message, map, defaultChoice);
	}

	@Override
	public void println(String message) {
		m_resource.write(new BootstrapMessageFactory().createAsBytes("feedback", message));
	}

	class StatusMessage {
		private final String type = "status";
		private final int current = 0;
		private final int max = 0;

		public StatusMessage(int current, int max) {
		}

		public String getType() {
			return type;
		}

		public int getCurrent() {
			return current;
		}

		public int getMax() {
			return max;
		}
	}

	@Override
	public void printf(String message, Object... args) {
		println(String.format(message, args));
	}

	@Override
	public void printStatus(int current, int max) {
		m_resource.write(new BootstrapMessageFactory().createAsBytes("status", new StatusMessage(0, 0)));
	}
}
