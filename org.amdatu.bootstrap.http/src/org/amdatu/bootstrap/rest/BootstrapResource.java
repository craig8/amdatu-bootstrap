/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.rest;

import java.io.File;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.amdatu.bootstrap.command.ProjectBndFile;
import org.amdatu.bootstrap.command.RunConfig;
import org.amdatu.bootstrap.command.WorkspaceBundleSelect;
import org.amdatu.bootstrap.core.BootstrapPlugin;
import org.amdatu.bootstrap.core.EnumValue;
import org.amdatu.bootstrap.core.PluginDescription;
import org.amdatu.bootstrap.core.PluginRegistry;
import org.amdatu.bootstrap.services.Navigator;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;

@Path("bootstrap")
@Component(provides = Object.class)
public class BootstrapResource {

	@ServiceDependency
	private volatile PluginRegistry m_pluginRegistry;

	@ServiceDependency
	private volatile Navigator m_navigator;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<PluginDescription> listPlugins() {
		return m_pluginRegistry.listPlugins();
	}

	@GET
	@Path("listdirs")
	@Produces(MediaType.APPLICATION_JSON)
	public List<File> ls() {
		List<File> asList = Arrays.asList(m_navigator.getCurrentDir().toFile().listFiles(f -> f.isDirectory()));
		return asList;
	}
	
	@GET
	@Path("listfiles")
	@Produces(MediaType.APPLICATION_JSON)
	public List<File> listFiles() {
		List<File> asList = Arrays.asList(m_navigator.getCurrentDir().toFile().listFiles(f -> f.isFile()));
		return asList;
	}
	
	@GET
	@Path("pwd")
	@Produces(MediaType.APPLICATION_JSON)
	public String pwd() {
		return m_navigator.getCurrentDir().toFile().getAbsolutePath();
	}
	
	@GET
	@Path("currentscope")
	@Produces(MediaType.APPLICATION_JSON)
	public String currentscope() {
		return m_navigator.getCurrentScope().toString();
	}
	
	@GET
	@Path("options")
	@Produces(MediaType.APPLICATION_JSON)
	public List<?> getOptions(@QueryParam("plugin") String plugin, @QueryParam("callback") String callback) throws Exception {
		if(callback.equals(RunConfig.class.getName())) {
			
			return m_navigator.findWorkspaceRunConfigs().stream()
					.sorted((a,b) -> a.endsWith("bndrun") ? 1 : -1)
					.map(p -> new EnumValue(p.toFile().getAbsolutePath(), p.getParent().toFile().getName() + " " + File.separator + " " + p.toFile().getName()))
					.collect(Collectors.toList());
		} else if(callback.equals(WorkspaceBundleSelect.class.getName())) {
			return m_navigator.findWorkspaceBundles();
		} else if(callback.equals(ProjectBndFile.class.getName())) {
			return m_navigator.findProjectBndFiles();
		} else {
			BootstrapPlugin bootstrapPlugin = m_pluginRegistry.getPlugin(plugin).get();
			Method method = bootstrapPlugin.getClass().getMethod(callback);
			return (List<?>) method.invoke(bootstrapPlugin);
		}
	}
	
	@GET
	@Path("recentworkspaces")
	@Produces(MediaType.APPLICATION_JSON)
	public List<String> getRecentWorkspaces() {
		return m_navigator.getRecentWorkspaces();
	}
	
	@GET
	@Path("ping")
	@Produces(MediaType.APPLICATION_JSON)
	public boolean ping() {
		return true;
	}
	
	@GET
	@Path("filesystemroots")
	@Produces(MediaType.APPLICATION_JSON) 
	public List<String> fileSystems(){
		 File[] listRoots = File.listRoots();
		return Arrays.stream(listRoots)
				 .map(File::toString)
				 .collect(Collectors.toList());
	}
}
