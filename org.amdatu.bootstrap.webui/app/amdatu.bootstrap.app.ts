// <reference path="typeScriptDefenitions/libs.d.ts" />
/// <amd-dependency path="angular-bootstrap"/>
/// <amd-dependency path="jquery"/>
/// <amd-dependency path="lodash"/>

import angular = require('angular')
import PluginsController = require('plugins/PluginsController')
import PluginsService = require('plugins/PluginsService')
import NavigationController = require('navigation/NavigationController')
import FeedbackModalController = require('plugins/FeedbackModalController')
import Directives = require('directives/BootstrapDirectives')
import Config = require('Config')
import _ = require('lodash')

var ngModule: ng.IModule = angular.module('amdatu.bootstrap.app', ['ui.bootstrap']);

ngModule.controller('PluginsController', PluginsController);
ngModule.controller('NavigationController', NavigationController);
ngModule.controller('FeedbackModalController', FeedbackModalController);
ngModule.directive('hotkeys', () => { return new Directives['HotkeyDirective'](); });
ngModule.service('PluginsService', PluginsService);
ngModule.constant('BASE_URL', Config.host);
ngModule.filter('commandScope', function() {
  return function(commands : any[], currentScope : string) {
    return _.filter(commands, command => command['scope'] == currentScope || command['scope'] == 'GLOBAL' || (command['scope'] == 'WORKSPACE' && currentScope == 'PROJECT'));
  }
});

console.log('config', Config.host);
export = ngModule;

