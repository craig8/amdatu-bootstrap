// <reference path="typeScriptDefenitions/libs.d.ts" />

class FeedbackModalController {
    static $inject = ['$scope', '$modalInstance', 'question'];
    private allSelected = false;

    constructor(private scope : ng.IScope, $modalInstance : ng.ui.bootstrap.IModalServiceInstance, question) {

        console.log("Question: ", question);

        scope['question'] = question;
        scope['answer'] = {};

        scope['ok'] = function () {
            if(scope['answer'].selectedItem) {
              $modalInstance.close(scope['answer'].selectedItem);
            } else {
              console.log(scope['answer'])
              $modalInstance.close(scope['answer']);

            }
        }

        scope['cancel'] = function() {
          $modalInstance.dismiss('cancel');
        }


      scope['selectAllCheckboxes'] = function() {
          this.allSelected = !this.allSelected;
          for(var key in scope['question'].questions) {
            scope['answer'][key] = this.allSelected;
          }
      }
    }


}

export = FeedbackModalController
