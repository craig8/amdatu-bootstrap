/// <reference path="./CommandArgument.d.ts" />
interface CommandDescription {
    name : string;
    arguments : CommandArgument[];
}