import OutputMessage = require("plugins/OutputMessage")

class DependenciesOutputMessage extends OutputMessage {
  dependencies : string[];

  constructor(message : string, dependencies : string[]) {
    super(message, 2);

    this.dependencies = dependencies;
  }

}

export = DependenciesOutputMessage

