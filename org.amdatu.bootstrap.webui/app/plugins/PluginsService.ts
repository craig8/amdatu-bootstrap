/// <reference path="../typescriptDefinitions/libs.d.ts" />

import Rx = require('Rx')
import SockJS = require('SockJS')
import Stomp = require('Stomp')
import Atmosphere = require('Atmosphere')

class PluginsService {
    static $inject = ['$http', '$q', 'BASE_URL', '$rootScope'];

    private request;

    constructor(private $http:ng.IHttpService, private $q:ng.IQService, private BASE_URL, private $rootScope : ng.IRootScopeService) {
    }

    getPlugins() : Rx.Observable<Plugin> {
        var observable = Rx.Observable.create((observer : Rx.Observer<Plugin>) => {
            this.$http.get(this.BASE_URL + "/bootstrap").success((resp) => {
                Rx.Observable.fromArray(resp).subscribe(
                    p => observer.onNext(<Plugin>p),
                    e => console.log(e),
                    () => observer.onCompleted());
            });
        });

        return observable;
    }

    ls() : Rx.Observable<string> {
        var observable = Rx.Observable.create((observer : Rx.Observer<string>) => {
            this.$http.get(this.BASE_URL + "/bootstrap/listdirs").success((resp) => {
                Rx.Observable.fromArray(resp).subscribe(
                    p => observer.onNext(<string>p),
                    e => console.log(e),
                    () => observer.onCompleted());
            });
        });

        return observable;
    }

    pwd():Rx.Observable<string> {
      var observable = Rx.Observable.create((observer:Rx.Observer<string>) => {
        this.$http.get(this.BASE_URL + "/bootstrap/pwd").success((resp) => {
          observer.onNext(resp);
          observer.onCompleted();
        });
      });

      return observable;
    }

    getCurrentScope() : Rx.Observable<string> {
      var observable = Rx.Observable.create((observer:Rx.Observer<string>) => {
        this.$http.get(this.BASE_URL + "/bootstrap/currentscope").success((resp) => {
          observer.onNext(resp);
          observer.onCompleted();
        });
      });

      return observable;
    }

    getRecentWorkspaces() : Rx.Observable<string[]> {
      var observable = Rx.Observable.create((observer:Rx.Observer<string[]>) => {
        this.$http.get(this.BASE_URL + "/bootstrap/recentworkspaces").success((resp) => {
          observer.onNext(resp);
          observer.onCompleted();
        });
      });

      return observable;
    }

    execute(command : string, argValues : {}) {
        if(argValues['arguments'] != undefined) {
            argValues['arguments'] = argValues['arguments'].split(" ");
        }

        this.request.push(JSON.stringify({name: command, args: argValues, type: 'command'}));

    }

    navigate(dir : string) {
        this.execute('navigation-cd', {dir: dir});
    }

    listFiles() : Rx.Observable<string[]> {
      var observable = Rx.Observable.create((observer : Rx.Observer<string[]>) => {
        this.$http.get(this.BASE_URL + "/bootstrap/listfiles").success((resp) => {
            observer.onNext(resp);
            observer.onCompleted();
        });
      });

      return observable;
    }

    listDrives() : Rx.Observable<string[]> {
        var observable = Rx.Observable.create((observer : Rx.Observer<string[]>) => {
            this.$http.get(this.BASE_URL + "/bootstrap/filesystemroots").success((resp) => {
                observer.onNext(resp);
                observer.onCompleted();
            });
        });

        return observable;
    }

    sendAnswer(answer) {
        var data = JSON.stringify(answer);
        this.request.push(data);
    }

    sendCancel() {
      var msg = {
        type: 'cancel'
      }

      this.request.push(JSON.stringify(msg));
    }

    getOptions(plugin : string, callback : string) : Rx.Observable<any> {
      var observable = Rx.Observable.create((observer : Rx.Observer<string>) => {
        this.$http.get(this.BASE_URL + "/bootstrap/options?plugin=" + plugin + "&callback=" + callback).success((resp) => {
          observer.onNext(resp);
          observer.onCompleted();
        });
      });

      return observable;
    }

    ping() : Rx.Observable<boolean> {
      return Rx.Observable.create((observer : Rx.Observer<boolean>) => {
        this.$http.get(this.BASE_URL + "/bootstrap/ping").success(() => {
          observer.onNext(true);
          observer.onCompleted();
        }).error(() => {
          observer.onError(false);
          observer.onCompleted();
        })
      });
    }


    connect() : Rx.Observable<any>{
        var observable = Rx.Observable.create((observer : Rx.Observer<any>) => {

            this.request = Atmosphere.subscribe({
                url: 'http://localhost:8181/atmosphere/bootstrap',
                contentType : "application/json",
                transport: 'websocket',
                trackMessageLength: false,
                onOpen: (resp) => {
                },
                onError: (e) => console.log(e),
                onMessage: (m) => {
                    var value = JSON.parse(m.responseBody);
                    observer.onNext(value);
                },
                onClose: () => console.log("Close"),
                onClientTimeout: () => console.log('timeout'),
                onTransportFailure: () => console.log("failure"),
                logLevel: 'debug'
            });
        });

        return observable;
    }

}

export = PluginsService;
