/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.plugins.licenseheaders;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.amdatu.bootstrap.command.Command;
import org.amdatu.bootstrap.command.Parameters;
import org.amdatu.bootstrap.core.BootstrapPlugin;
import org.amdatu.bootstrap.services.DependencyBuilder;
import org.amdatu.bootstrap.services.Navigator;
import org.amdatu.bootstrap.services.Prompt;
import org.amdatu.template.processor.TemplateContext;
import org.amdatu.template.processor.TemplateEngine;
import org.amdatu.template.processor.TemplateProcessor;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.Inject;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.osgi.framework.BundleContext;

@Component
public class LicenseHeadersPlugin implements BootstrapPlugin{
	
	@ServiceDependency(required = true)
	private volatile TemplateEngine m_templateEngine;

	@ServiceDependency
	private volatile Navigator m_navigator;

	@ServiceDependency
	private volatile DependencyBuilder m_dependencyBuilder;
	
	@Inject
	private volatile BundleContext m_bundleContext;
	
	interface SourceHeaderArgs extends Parameters {
		File source();
	}
	
	@Command
	public void addheaders(SourceHeaderArgs args, Prompt prompt) throws Exception{
			URL templateUri = m_bundleContext.getBundle().getEntry("/templates/amdatu.vm");
			TemplateProcessor processor = m_templateEngine.createProcessor(templateUri);
			TemplateContext context = m_templateEngine.createContext();
			context.put("copyrightYears", LocalDate.now().getYear());
			String template = processor.generateString(context);

			
			if(args.source() == null) {
				addHeadersToFileTree(prompt, template);
			} else {
				File sourceFile = m_navigator.getCurrentDir().resolve(args.source().toString()).toFile();
				if(fileNeedsHeader(sourceFile, prompt)) {
					addHeader(sourceFile, template, prompt);
				}
			}
			
	}

	private void addHeadersToFileTree(Prompt prompt, String template) throws IOException {
		List<File> files = Files.walk(m_navigator.getCurrentDir(), Integer.MAX_VALUE)
			.filter(p -> !p.toString().contains(File.separator + "."))
			.filter(p -> p.toString().endsWith(".java"))
			.map(Path::toFile)
			.filter(f -> fileNeedsHeader(f, prompt))
			.collect(Collectors.toList());
		
		List<String> fileNames = files.stream().map(File::getName).collect(Collectors.toList());
		
		Map<String, Boolean> result = prompt.askMultipleBoolean("Add license header?", fileNames, true);
		
		result.entrySet().stream()
			.filter(e -> e.getValue())
			.map(e -> files.stream().filter(f -> f.getName().equals(e.getKey())).findAny())
			.filter(Optional::isPresent)
			.forEach(f -> addHeader(f.get(), template, prompt));
	}
	
	private boolean fileNeedsHeader(File sourceFile, Prompt prompt) {
		try(BufferedReader br = new BufferedReader(new FileReader(sourceFile))) {
			String line = br.readLine();
			return !line.equals("/*");
		} catch(Exception ex) {
			prompt.printf("Error reading file %s", sourceFile.getName());
		}
		
		return false;
	}

	private void addHeader(File sourceFile, String template, Prompt prompt)  {
		File targetFile = new File(sourceFile.getParentFile(), sourceFile.getName() + ".new");
		try(BufferedReader br = new BufferedReader(new FileReader(sourceFile));
				BufferedWriter	bw = new BufferedWriter(new FileWriter(targetFile))) {
			bw.write(template);
			bw.newLine();
			String line = br.readLine();
			
			while (line != null) {
				bw.write(line);
				bw.newLine();
				line = br.readLine();
			}

			if (sourceFile.delete()) {
				targetFile.renameTo(sourceFile);
				prompt.printf("Added license header to: %s", sourceFile.getName());
			}
			else {
				targetFile.delete();
				throw new IOException("Could not delete source file (to replace it with an update with the header).");
			}
		} catch(Exception ex) {
			prompt.printf("Error adding license header to file %s: %s ", sourceFile.getName(), ex.getMessage());
			ex.printStackTrace();
		}
	}

	@Override
	public String getName() {
		return "licenseheaders";
	}
}
