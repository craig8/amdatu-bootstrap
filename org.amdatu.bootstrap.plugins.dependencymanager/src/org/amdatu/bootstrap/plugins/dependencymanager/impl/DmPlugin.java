/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.plugins.dependencymanager.impl;

import java.io.File;

import org.amdatu.bootstrap.command.Command;
import org.amdatu.bootstrap.command.Description;
import org.amdatu.bootstrap.command.InstallResult;
import org.amdatu.bootstrap.command.Parameters;
import org.amdatu.bootstrap.command.Required;
import org.amdatu.bootstrap.command.RunConfig;
import org.amdatu.bootstrap.command.Scope;
import org.amdatu.bootstrap.command.StoreValue;
import org.amdatu.bootstrap.core.BootstrapPlugin;
import org.amdatu.bootstrap.plugins.dependencymanager.DmService;
import org.amdatu.bootstrap.services.Navigator;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;

@Component
public class DmPlugin implements BootstrapPlugin {

	@ServiceDependency
	private volatile DmService m_dmService;
	
	@ServiceDependency
	private volatile Navigator m_navigator;
	
	interface DmInstallParameters extends Parameters {
		@StoreValue
		boolean useAnnotations();
	}
	
	@Command(scope=Scope.PROJECT)
	public InstallResult install(DmInstallParameters params) {
		boolean useAnnotations = params.useAnnotations();
		
		InstallResult annotationsInstallResult = null;
		if(useAnnotations) {
			annotationsInstallResult = m_dmService.installAnnotationProcessor();
		}
		
		return InstallResult.builder()
				.addResult(annotationsInstallResult)
				.addResult(m_dmService.addDependencies()).build();
	}
	
	interface DmRunParameters extends Parameters {
		@Required
		@RunConfig
		@Description("Run configuration to add dependencies to")
		File runConfig();
	}
	
	@Command(scope=Scope.PROJECT)
	public InstallResult run(DmRunParameters params) {
		return m_dmService.addRunDependencies(params.runConfig().toPath());
	}

	@Override
	public String getName() {
		return "dependencymanager";
	}
}
