/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.plugins.itest;

import java.io.File;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;

import org.amdatu.bootstrap.command.Command;
import org.amdatu.bootstrap.command.Description;
import org.amdatu.bootstrap.command.FullyQualifiedName;
import org.amdatu.bootstrap.command.Parameters;
import org.amdatu.bootstrap.command.Scope;
import org.amdatu.bootstrap.command.WorkspaceBundleSelect;
import org.amdatu.bootstrap.core.BootstrapPlugin;
import org.amdatu.bootstrap.services.Dependency;
import org.amdatu.bootstrap.services.DependencyBuilder;
import org.amdatu.bootstrap.services.Navigator;
import org.amdatu.bootstrap.services.ProjectConfigurator;
import org.amdatu.template.processor.TemplateContext;
import org.amdatu.template.processor.TemplateEngine;
import org.amdatu.template.processor.TemplateProcessor;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.Inject;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.osgi.framework.BundleContext;

@Component
public class ITestPlugin implements BootstrapPlugin{
	
	@ServiceDependency
	private volatile Navigator m_navigator;
	
	@ServiceDependency 
	private volatile DependencyBuilder m_dependencyBuilder;
	
	@ServiceDependency
	private volatile TemplateEngine m_templateEngine;
	
	@ServiceDependency
	private volatile ProjectConfigurator m_projectConfigurator;
	
	
	@Inject
	private volatile BundleContext m_bundleContext;

	@Override
	public String getName() {
		return "itest";
	}
	
	interface TestParams extends Parameters {
		
		@Description("Bundle containing the service to test")
		@WorkspaceBundleSelect
		String project();
		
		@Description("Fully qualified name of the service interface to test")
		String serviceToTest(); 
		
		@Description("Use Mongo testing support")
		boolean useMongo();
	}
	
	@Command(scope=Scope.PROJECT)
	public File createTest(TestParams params) {
		m_dependencyBuilder.addDependency(params.project(), "latest");
		m_dependencyBuilder.addRunDependency(params.project(), m_navigator.getBndFile());
		
		if(params.useMongo()) {
			m_dependencyBuilder.addDependencies(Dependency.fromStrings(
					"org.amdatu.testing.mongo;version='[1.0.2, 2)'", 
					"org.mongodb.mongo-java-driver",
					"org.amdatu.mongo"));
			
			m_dependencyBuilder.addRunDependency(Dependency.fromStrings(
					"org.amdatu.testing.mongo;version='[1.0.2, 2)'", 
					"org.mongodb.mongo-java-driver",
					"org.amdatu.mongo"), m_navigator.getBndFile());
		}
		
		FullyQualifiedName fqnToTest = new FullyQualifiedName(params.serviceToTest());
		FullyQualifiedName fqn = new FullyQualifiedName(fqnToTest.getPackageName() + ".test." + fqnToTest.getClassName() + "Test");

		m_projectConfigurator.addEntryToHeader(m_navigator.getBndFile(), "Test-Cases", fqn.getFull());
		
		URL templateUri = m_bundleContext.getBundle().getEntry("/templates/itest.vm");
		
		try {
			TemplateProcessor processor = m_templateEngine.createProcessor(templateUri);
			TemplateContext context = m_templateEngine.createContext();
			context.put("componentName", fqn.getClassName());
			context.put("packageName", fqn.getPackageName());
			context.put("interfaceToTest", fqnToTest);
			context.put("useMongo", params.useMongo());

			String template = processor.generateString(context);
			Path outputDir = m_navigator.getCurrentDir().resolve("src")
					.resolve(fqn.getPackageName().replaceAll("\\.", "/"));
			Path outputFile = outputDir.resolve(fqn.getClassName() + ".java");
			Files.createDirectories(outputDir);
			
			Files.write(outputFile, template.getBytes());
			
			m_projectConfigurator.addPrivatePackage(m_navigator.getBndFile(), fqn.getPackageName());
			
			return outputFile.toFile();
		} catch(Exception ex) {
			throw new RuntimeException(ex);
		}
	}

}
