/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.plugins.gradle;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.amdatu.bootstrap.gradle.GradleService;
import org.amdatu.bootstrap.services.Navigator;
import org.amdatu.bootstrap.services.Prompt;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;

@Component
public class GradleServiceImpl implements GradleService{
	
	private static String OS = System.getProperty("os.name").toLowerCase();
	
	@ServiceDependency
	private volatile Navigator m_navigator;
	
	@Override
	public void build(Prompt prompt, String... args) {
		build(m_navigator.getWorkspaceDir(), prompt, args);
	}
	
	@Override
	public void build(Path baseDir, Prompt prompt, String... args) {
		List<String> gradleCmd = new ArrayList<>();
        if (isWindows()) {
            gradleCmd.add("gradlew.bat");
        } else {
            gradleCmd.add("sh");
            gradleCmd.add("gradlew");
        }
        
        if(args != null) {
        	gradleCmd.addAll(Arrays.asList(args));
        }
        startProcess(prompt, baseDir, gradleCmd.toArray(new String[0]));
	}
	
	private void startProcess(Prompt prompt, Path workingDirectory, String... command) {
        ProcessBuilder pb = new ProcessBuilder(command);
        pb.directory(workingDirectory.toFile());
        // We want the sys err in the out so we can read them both
        pb.redirectErrorStream(true);
        
        String path = pb.environment().get("PATH");
        pb.environment().put("PATH", path + ":/usr/local/bin");
        try {
            Process p = pb.start();
            BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line;
            while ((line = input.readLine()) != null) {
            	   System.out.println(line);
                prompt.println(line);
             
            }
            input.close();
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
	
	private boolean isWindows() {
        return (OS.indexOf("win") >= 0);
    }

	
}
