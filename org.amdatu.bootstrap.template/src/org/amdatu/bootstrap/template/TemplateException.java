/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.template;

/**
 * The exception that is thrown by the template engine if in exception occurs,
 * instead of that exception, or if an other error occurs. Most API methods throw
 * this exception.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class TemplateException extends Exception {

    private static final long serialVersionUID = 9067873330488395833L;

    /**
     * Constructs a default TemplateException.
     */
    public TemplateException() {
        super();
    }

    /**
     * Constructs a template exception with the specified string as its message.
     * 
     * @param msg The error message
     */
    public TemplateException(String msg) {
        super(msg);
    }

    /**
     * Constructs a template exception with the specified throwable as the cause of
     * this exception.
     * 
     * @param cause The cause
     */
    public TemplateException(Throwable cause) {
        super(cause);
    }

    /**
     * Constructs a template exception with the specified string as its message and the
     * throwable as the cause of the exception.
     * 
     * @param msg The message
     * @param cause The cause
     */
    public TemplateException(String msg, Throwable cause) {
        super(msg, cause);
    }

}
