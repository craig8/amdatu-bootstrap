/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.plugins.project;

import java.io.File;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.amdatu.bootstrap.command.Command;
import org.amdatu.bootstrap.command.DescribedEnum;
import org.amdatu.bootstrap.command.Description;
import org.amdatu.bootstrap.command.DynamicSelect;
import org.amdatu.bootstrap.command.Parameters;
import org.amdatu.bootstrap.command.Required;
import org.amdatu.bootstrap.command.Scope;
import org.amdatu.bootstrap.core.BootstrapPlugin;
import org.amdatu.bootstrap.core.EnumValue;
import org.amdatu.bootstrap.services.Navigator;
import org.amdatu.bootstrap.services.Prompt;
import org.amdatu.bootstrap.services.ResourceManager;
import org.amdatu.bootstrap.template.Template;
import org.amdatu.bootstrap.template.TemplateException;
import org.amdatu.bootstrap.template.TemplateProcessor;
import org.amdatu.bootstrap.template.TemplateProvider;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventAdmin;

import aQute.bnd.build.model.BndEditModel;
import aQute.bnd.osgi.Builder;
import aQute.bnd.properties.Document;
import aQute.bnd.version.Version;

@Component
public class ProjectPlugin implements BootstrapPlugin {
	
	@ServiceDependency
	private EventAdmin m_eventAdmin;
	
	@ServiceDependency
	private volatile Navigator m_navigator;
	
	@ServiceDependency
	private volatile TemplateProcessor m_templateProcessor;
	
	private final Set<TemplateProvider> m_projectTemplateProviders = new HashSet<>();
	
	@ServiceDependency
	private volatile ResourceManager m_resourceManager;
	
	interface CreateParameters extends Parameters {
		@Description("Name of the project")
		@Required
		String name();
		
		@Description("Name of the template")
		@DynamicSelect("templates")
		String template();
	}
	
	public List<EnumValue> templates() {
		return m_projectTemplateProviders.stream().flatMap(p -> p.listTemplates().stream()).map(t -> new EnumValue(t.getName(), t.getName())).collect(Collectors.toList());
	}
	
	@Command(scope=Scope.WORKSPACE)
	public File create(CreateParameters parameters, Prompt prompt) {
		if (m_navigator.getProjectDir() != null){
			prompt.println("Can't create project (already in a project)");
			return null;
		}
		
		File file = new File(m_navigator.getCurrentDir().toFile(), parameters.name());
		if (file.exists()){
			prompt.println("Can't create project (file exists)");
			return null;
		}
		
		if (!file.mkdir()){
			prompt.println("Can't create project (couldn't create project dir)");
			return null;
		}
		
		try {
			Template template = determineTemplate(parameters, prompt, m_projectTemplateProviders);
			
			Map<String, Object> context = new HashMap<>();
			context.put("projectName", parameters.name());

			m_templateProcessor.installTemplate(template, file, context);
			
			m_navigator.changeDir(file.toPath());
			
			prompt.println(String.format("Created new project '%s' using template '%s'", parameters.name(), template.getName()));

			
			sendEvent("org/amdatu/bootstrap/core/PROJECT_CREATED", parameters.name());

		} catch (TemplateException e) {
			throw new RuntimeException(e);
		}
		
		return null;
	}

	private Template determineTemplate(CreateParameters parameters, Prompt prompt, Set<TemplateProvider> templateProviders) {
		List<Template> templateOptions = new ArrayList<>();
		
		
		for (TemplateProvider p : templateProviders){
			templateOptions.addAll(p.listTemplates());
		}

		Template template = null;
		if (parameters.template() != null){
			for (Template t : templateOptions) {
				if (t.getName().equals(parameters.template())){
					template = t;
					break;
				}
			}
			if (template == null){
				prompt.println(String.format("Template '%s' not found", parameters.template()));
			}
			
		} else {
			if (templateOptions.size() == 0){
				prompt.println("No project templates found");
			} else if (templateOptions.size() == 1){
				template = templateOptions.get(0);
			} else {
				template = prompt.askChoice("Which template do you want to use", 0, templateOptions);
			}
			
		}
		return template;
	}
	
	interface VersionBumpParams extends Parameters{
		@Description("Part of the version to bump")
		@Required
		BumpPolicy bumpPolicy();
	}
	
	enum BumpPolicy implements DescribedEnum{
		MAJOR("Major"),
		MINOR("Minor"),
		MICRO("Micro");
		
		private final String m_description;
		
		BumpPolicy(String description) {
			m_description = description;
		}

		@Override
		public String getDescription() {
			return m_description;
		}
	}
	
	@Command(scope=Scope.WORKSPACE)
	public void bumpVersion(VersionBumpParams params, Prompt prompt) {
		if(m_navigator.getCurrentProject() != null) {
			try {
				m_navigator.getCurrentProject().getSubBuilders()
					.forEach(b -> fixProject(b, params.bumpPolicy()));
				
			} catch (Exception e) {
				e.printStackTrace();
				throw new RuntimeException(e);
			}
		} else {
			boolean confirmed = prompt.askBoolean("Are you sure you want to bump all projects in the workspace?", false);
			
			if(confirmed) {
				try {
					m_navigator.getCurrentWorkspace().getAllProjects().stream().forEach(p -> {
						try {
							p.getSubBuilders().forEach(b -> fixProject(b, BumpPolicy.MICRO));
						} catch (Exception e) {
							e.printStackTrace();
							
							throw new RuntimeException(e);
						}
					});
				} catch (Exception e) {
					e.printStackTrace();
					throw new RuntimeException(e);
				}
			}
		}
	}
	
	private void fixProject(Builder builder, BumpPolicy versionPart) {
        try {
            
            BndEditModel model = new BndEditModel();
            File bndFile = builder.getPropertiesFile();
            if(bndFile == null) {
            	bndFile = Builder.getFile(builder.getBase(), "bnd.bnd");
            }

			model.loadFrom(bndFile);
			model.setBundleVersion(calculateNewVersion(model.getBundleVersionString(), versionPart).toString());
   
            Document document = new Document(new String(Files.readAllBytes(bndFile.toPath())));
            model.saveChangesTo(document);
   
            String documentContents = document.get();
            m_resourceManager.writeFile(bndFile.toPath(), documentContents.getBytes());
            
            
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
	 
	}
	
	private Version calculateNewVersion(String current, BumpPolicy policy) {
		if(current == null) {
			current = "0.0.0";
		}
		
        Version v = new Version(current);
        int major = v.getMajor();
        int minor = v.getMinor();
        int micro = v.getMicro();
        
        switch(policy) {
        case MAJOR: major++;
        	minor = 0;
        	micro = 0;
        	break;
        case MINOR: minor++;
        	micro = 0;
        	break;
        default: micro++;
        }
        
        return new Version(major, minor, micro);
	}
		
	@ServiceDependency(removed="templateProviderRemoved", filter="(type=project)")
	private void templateProviderAdded(TemplateProvider templateProvider){
		m_projectTemplateProviders.add(templateProvider);
	}
	
	@SuppressWarnings("unused" /* dependency manager callback */)
	private void templateProviderRemoved(TemplateProvider templateProvider) {
		m_projectTemplateProviders.remove(templateProvider);
	}
	
	private void sendEvent(String topicName, String name) {
	    Map<String, Object> props = new HashMap<>();
	    props.put("projectname", name);
	    Event event = new Event(topicName, props);
	    m_eventAdmin.sendEvent(event);
	}

	@Override
	public String getName() {
		return "project";
	}
}
