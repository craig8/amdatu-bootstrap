/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.plugins.amdatu.rest;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import org.amdatu.bootstrap.command.Command;
import org.amdatu.bootstrap.command.Description;
import org.amdatu.bootstrap.command.FullyQualifiedName;
import org.amdatu.bootstrap.command.InstallResult;
import org.amdatu.bootstrap.command.Parameters;
import org.amdatu.bootstrap.command.Required;
import org.amdatu.bootstrap.command.RunConfig;
import org.amdatu.bootstrap.command.Scope;
import org.amdatu.bootstrap.command.InstallResult.Builder;
import org.amdatu.bootstrap.core.BootstrapPlugin;
import org.amdatu.bootstrap.plugins.dependencymanager.DmService;
import org.amdatu.bootstrap.services.Dependency;
import org.amdatu.bootstrap.services.DependencyBuilder;
import org.amdatu.bootstrap.services.Navigator;
import org.amdatu.template.processor.TemplateContext;
import org.amdatu.template.processor.TemplateEngine;
import org.amdatu.template.processor.TemplateException;
import org.amdatu.template.processor.TemplateProcessor;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.Inject;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.osgi.framework.BundleContext;

@Component
public class RestPlugin implements BootstrapPlugin{

	@ServiceDependency
	private volatile DependencyBuilder m_dependencyBuilder;
	
	@ServiceDependency
	private volatile DmService m_dmService;	
	
	@ServiceDependency
	private volatile Navigator m_navigator;
	
	@ServiceDependency
	private volatile TemplateEngine m_templateEngine;
	
	@Inject
	private volatile BundleContext m_bundleContext;
	
	@Command(scope=Scope.PROJECT)
	public InstallResult install() {
		return m_dependencyBuilder.addDependencies(
				Dependency.fromStrings(
						"org.amdatu.web.rest.jaxrs", 
						"org.amdatu.web.rest.doc", 
						"javax.servlet", 
						"com.fasterxml.jackson.core.jackson-annotations",
						"com.fasterxml.jackson.core.jackson-core",
						"com.fasterxml.jackson.core.jackson-databind"));
	}

	interface RestRunArguments extends Parameters {
		
		@Required
		@RunConfig
		@Description("Run configuration to add dependencies to")
		File bndRunFile();
	}
	
	@Command(scope=Scope.PROJECT)
	public InstallResult run(RestRunArguments args) {
		List<Dependency> deps = Dependency.fromStrings(
				"org.apache.felix.http.jetty",
				"org.apache.felix.http.api",
				"org.apache.felix.http.servlet-api",
				"org.apache.felix.http.whiteboard", 
				"org.amdatu.web.rest.jaxrs", 
				"org.amdatu.web.rest.wink",
				"org.amdatu.web.rest.doc", 
				"com.fasterxml.jackson.core.jackson-annotations",
				"com.fasterxml.jackson.core.jackson-core",
				"com.fasterxml.jackson.core.jackson-databind",
				"com.fasterxml.jackson.jaxrs.jackson-jaxrs-base",
				"com.fasterxml.jackson.jaxrs.jackson-jaxrs-json-provider"
				);
		
		Builder builder = InstallResult.builder();
		
		Path path = args.bndRunFile().toPath();
		builder.addResult(m_dependencyBuilder.addRunDependency(deps, path));
		builder.addResult(m_dmService.addRunDependencies(path));
		
		return builder.build();
	}

	interface CreateComponentArguments extends Parameters{
		FullyQualifiedName fqn();
		String path();
		boolean annotations();
	}
	
	@Command(scope=Scope.PROJECT)
	public File createcomponent(CreateComponentArguments args) {
		FullyQualifiedName fqn = args.fqn();
		String path = args.path();
		boolean useAnnotations = args.annotations();
		

		URL templateUri = m_bundleContext.getBundle().getEntry("/templates/restcomponent.vm");
		
		try {
			TemplateProcessor processor = m_templateEngine.createProcessor(templateUri);
			TemplateContext context = m_templateEngine.createContext();
			context.put("componentName", fqn.getClassName());
			context.put("packageName", fqn.getPackageName());
			context.put("useAnnotations", useAnnotations);
			context.put("path", path);

			String template = processor.generateString(context);
			Path outputDir = m_navigator.getCurrentDir().resolve("src")
					.resolve(fqn.getPackageName().replaceAll("\\.", "/"));
			Path outputFile = outputDir.resolve(fqn.getClassName() + ".java");
			Files.createDirectories(outputDir);
			
			Files.write(outputFile, template.getBytes());
			
			 if (useAnnotations) {
				 m_dmService.installAnnotationProcessor();
			 } else {
				 m_dmService.createActivator(fqn, new FullyQualifiedName("java.lang.Object"));
			 }
			 
			 m_dmService.addDependencies();
			
			return outputFile.toFile();
		} catch (TemplateException | IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public String getName() {
		return "rest";
	}	
	
}
