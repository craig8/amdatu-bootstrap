package org.amdatu.bootstrap.plugins.amdatu.scheduling;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import org.amdatu.bootstrap.command.Command;
import org.amdatu.bootstrap.command.Description;
import org.amdatu.bootstrap.command.FullyQualifiedName;
import org.amdatu.bootstrap.command.InstallResult;
import org.amdatu.bootstrap.command.Parameters;
import org.amdatu.bootstrap.command.Required;
import org.amdatu.bootstrap.command.RunConfig;
import org.amdatu.bootstrap.core.BootstrapPlugin;
import org.amdatu.bootstrap.plugins.dependencymanager.DmService;
import org.amdatu.bootstrap.services.Dependency;
import org.amdatu.bootstrap.services.DependencyBuilder;
import org.amdatu.bootstrap.services.Navigator;
import org.amdatu.template.processor.TemplateContext;
import org.amdatu.template.processor.TemplateEngine;
import org.amdatu.template.processor.TemplateException;
import org.amdatu.template.processor.TemplateProcessor;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.Inject;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.osgi.framework.BundleContext;

@Component
public class SchedulingPlugin implements BootstrapPlugin{

	private static final List<Dependency> DEPENDENCIES = Dependency.fromStrings("org.amdatu.scheduling.api", "org.amdatu.scheduling.quartz");
	@ServiceDependency
	private volatile DependencyBuilder m_dependencies;
	
	@ServiceDependency
	private volatile TemplateEngine m_templateEngine;
	
	@ServiceDependency
	private volatile DmService m_dmService;
	
	@ServiceDependency
	private volatile Navigator m_navigator;
	
	@Inject
	private volatile BundleContext m_bundleContext;
	
	@Command
	public InstallResult install() {
		return m_dependencies.addDependencies(DEPENDENCIES);
	}
	
	interface RunParams extends Parameters{
		@RunConfig
		@Required
		File runConfig();
	}
	
	@Command
	public InstallResult run(RunParams params) {
		return m_dependencies.addRunDependency(DEPENDENCIES, params.runConfig().toPath());
	}
	
	interface ComponentParams extends Parameters{
		FullyQualifiedName fullyQualifiedName();
		
		@Description("Use Dependency Manager annotations")
		boolean annotations();
	}
	
	@Command
	public File createComponent(ComponentParams params) {
		FullyQualifiedName fqn = params.fullyQualifiedName();
		boolean useAnnotations = params.annotations();
		

		URL templateUri = m_bundleContext.getBundle().getEntry("/templates/schedulingcomponent.vm");
		
		try {
			TemplateProcessor processor = m_templateEngine.createProcessor(templateUri);
			TemplateContext context = m_templateEngine.createContext();
			context.put("componentName", fqn.getClassName());
			context.put("packageName", fqn.getPackageName());
			context.put("useAnnotations", useAnnotations);

			String template = processor.generateString(context);
			Path outputDir = m_navigator.getCurrentDir().resolve("src")
					.resolve(fqn.getPackageName().replaceAll("\\.", "/"));
			Path outputFile = outputDir.resolve(fqn.getClassName() + ".java");
			Files.createDirectories(outputDir);
			
			Files.write(outputFile, template.getBytes());
			
			 if (useAnnotations) {
				 m_dmService.installAnnotationProcessor();
			 } else {
				 m_dmService.createActivator(fqn, new FullyQualifiedName("org.quartz.Job"));
			 }
			 
			 m_dmService.addDependencies();
			
			return outputFile.toFile();
		} catch (TemplateException | IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	
	@Override
	public String getName() {
		return "scheduling";
	}

}
