/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.plugins.amdatu.jpa;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import org.amdatu.bootstrap.command.Command;
import org.amdatu.bootstrap.command.Description;
import org.amdatu.bootstrap.command.InstallResult;
import org.amdatu.bootstrap.command.InstallResult.Builder;
import org.amdatu.bootstrap.command.Parameters;
import org.amdatu.bootstrap.command.ProjectBndFile;
import org.amdatu.bootstrap.command.RunConfig;
import org.amdatu.bootstrap.command.Scope;
import org.amdatu.bootstrap.command.Select;
import org.amdatu.bootstrap.command.SelectOption;
import org.amdatu.bootstrap.core.BootstrapPlugin;
import org.amdatu.bootstrap.plugins.dependencymanager.DmService;
import org.amdatu.bootstrap.services.Dependency;
import org.amdatu.bootstrap.services.DependencyBuilder;
import org.amdatu.bootstrap.services.Navigator;
import org.amdatu.bootstrap.services.ResourceManager;
import org.amdatu.template.processor.TemplateContext;
import org.amdatu.template.processor.TemplateEngine;
import org.amdatu.template.processor.TemplateException;
import org.amdatu.template.processor.TemplateProcessor;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.Inject;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.osgi.framework.BundleContext;

import aQute.bnd.build.model.BndEditModel;
import aQute.bnd.properties.Document;

@Component
public class JpaPlugin implements BootstrapPlugin {

	@ServiceDependency
	private volatile DependencyBuilder m_dependencyBuilder;

	@ServiceDependency
	private volatile DmService m_dmService;
	
	@ServiceDependency
	private volatile TemplateEngine m_templateEngine;
	
	@Inject
	private volatile BundleContext m_bundleContext;
	
	@ServiceDependency
	private volatile Navigator m_navigator;

	@ServiceDependency
	private volatile ResourceManager m_resourceManager;
	
	interface InstallParams extends Parameters{
		@Description("Install a persistence provider")
		@Select({@SelectOption(value="hibernate", description="Use Hibernate persistence provider"),
					@SelectOption(value="openjpa", description="Use OpenJPA persistence provider"),
					@SelectOption(value="eclipselink", description="Use EclipdeLink persistence provider")})
		String provider();
	}
	
	
	@Command
	public InstallResult install(InstallParams params) {
		if(params.provider().equals("hibernate") || params.provider().equals("eclipselink")) {
			return m_dependencyBuilder.addDependency("org.amdatu.persistence2_1");
		} else {
			return m_dependencyBuilder.addDependency("org.amdatu.persistence2_0");
		}
	}
	
	interface RunParams extends Parameters{
		@Description("Location of run configuration")
		@RunConfig
		File runFile();
		
		@Description("Install a persistence provider")
		@Select({@SelectOption(value="hibernate", description="Use Hibernate persistence provider"),
					@SelectOption(value="openjpa", description="Use OpenJPA persistence provider"),
					@SelectOption(value="eclipselink", description="Use EclipdeLink persistence provider")})
		String provider();
		
	}
	
	@Command(scope=Scope.PROJECT)
	public InstallResult run(RunParams params) {
		
		return addRunDependencies(params);
	}
	
	interface PersistenceUnitParams extends Parameters{
		@Description("persistence unit name")
		String puName();
		
		@Description("managed data source name")
		String managedDsName();
		
		@Description("unmanaged data source name")
		String unManagedDsName();
		
		@Description("Persistence provider")
		@Select({@SelectOption(value="hibernate", description="Use Hibernate persistence provider"),
					@SelectOption(value="openjpa", description="Use OpenJPA persistence provider"),
					@SelectOption(value="eclipselink", description="Use EclipdeLink persistence provider")})
		String provider();
		
		@Description("bnd file to add persistence manafest headers")
		@ProjectBndFile
		File bndfile();
	}
	
	
	@Command(scope=Scope.PROJECT)
	public File persistencexml(PersistenceUnitParams params) {
		URL templateUri = m_bundleContext.getBundle().getEntry(getTemplateUrl(params.provider()));
		
		Path bndFile = params.bndfile() != null ? m_navigator.getCurrentDir().resolve(params.bndfile().toPath()) : m_navigator.getBndFile();
		
		addManifestHeaders(bndFile);
		
		try {
			TemplateProcessor processor = m_templateEngine.createProcessor(templateUri);
			TemplateContext context = m_templateEngine.createContext();
			context.put("persistenceUnitName", params.puName());
			context.put("managedDsName", params.managedDsName());
			context.put("unmanagedDsName", params.unManagedDsName());

			String template = processor.generateString(context);
			Path outputDir = m_navigator.getCurrentDir().resolve("persistence");
					
			Path outputFile = outputDir.resolve("persistence.xml");
			Files.createDirectories(outputDir);
			
			Files.write(outputFile, template.getBytes());
			
			return outputFile.toFile();
		} catch (TemplateException | IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	private void addManifestHeaders(Path bndFile) {
		try {
			BndEditModel model = new BndEditModel();

			model.loadFrom(bndFile.toFile());

			model.addIncludeResource("META-INF/persistence.xml=persistence/persistence.xml");
			model.genericSet("Meta-Persistence", "META-INF/persistence.xml");

			String bndContents = new String(Files.readAllBytes(bndFile));
			Document document = new Document(bndContents);
			model.saveChangesTo(document);

			m_resourceManager.writeFile(bndFile, document.get().getBytes());
		} catch (IOException e) {
			throw new RuntimeException("Error writing manifest headers to bnd file", e);
		}

	}

	private String getTemplateUrl(String provider) {
		switch(provider) {
			case "hibernate": 
				return "/templates/hibernate-persistencexml.vm";
			case "eclipselink": 
				return "/templates/eclipselink-persistencexml.vm";
			case "openjpa": 
				return "/templates/openjpa-persistencexml.vm";
			default: 
				throw new RuntimeException("No persistence provider install in project");
		}
	}
	
	private InstallResult addRunDependencies(RunParams params) {
		Builder builder = InstallResult.builder();

		Path path = params.runFile().toPath();
		builder.addResult(m_dependencyBuilder.addRunDependency(commonDependencies(), path));
		
		if("hibernate".equals(params.provider())) {
			builder.addResult(m_dependencyBuilder.addRunDependency(hibernateDependencies(), path));
		}
		
		if("openjpa".equals(params.provider())) {
			builder.addResult(m_dependencyBuilder.addRunDependency(openJpaDependencies(), path));
		}
		
		if("eclipselink".equals(params.provider())) {
			builder.addResult(m_dependencyBuilder.addRunDependency(eclipseLinkDependencies(), path));
		}

		builder.addResult(m_dmService.addRunDependencies(path));
		return builder.build();
	}
	
	private List<Dependency> commonDependencies() {
		return Dependency.fromStrings(
				"org.apache.commons.lang3",
				"org.amdatu.jpa.datasourcefactory",
				"org.amdatu.jpa.extender",
				"org.amdatu.jta.manager",
				"org.amdatu.jta.transactionmanager",
				"org.amdatu.jta.api"
);
	}
	
	private List<Dependency> hibernateDependencies() {
		return Dependency.fromStrings(
			"org.amdatu.persistence2_1",
			"org.hibernate.core",
			"org.hibernate.entitymanager",
			"org.apache.servicemix.bundles.antlr",
			"classmate",
			"javassist",
			"org.apache.servicemix.bundles.dom4j",
			"org.hibernate.common.hibernate-commons-annotations",
			"org.jboss.logging.jboss-logging",
			"org.jboss.jandex;version=latest");
	}
	
	private List<Dependency> openJpaDependencies() {
		return Dependency.fromStrings(
				"javax.servlet",
				"org.amdatu.persistence2_0;version=latest",
				"org.apache.commons.collections;version='[3.2.1,3.2.2)'",
				"org.apache.commons.lang;version='[2.6.0,2.6.1)'",
				"org.apache.commons.pool;version='[1.5.4,1.5.5)'",
				"org.apache.servicemix.bundles.commons-dbcp;version='[1.4.0.3,1.4.0.3]'",
				"org.apache.servicemix.bundles.serp;version='[1.14.1.1,1.14.1.1]'",
			  	"org.apache.xbean.asm5-shaded",
				"org.apache.openjpa;version='[2.4.0,2.5.0)'"
		);
	}
	
	private List<Dependency> eclipseLinkDependencies() {
		return Dependency.fromStrings(
				"org.amdatu.persistence2_1",
				"org.eclipse.persistence.antlr",
				"org.eclipse.persistence.asm",
				"org.eclipse.persistence.core",
				"org.eclipse.persistence.jpa",
				"org.eclipse.persistence.jpa.jpql",
				"org.amdatu.jpa.adapter.eclipselink;version=latest"
		);
	}
	
	

	@Override
	public String getName() {
		return "jpa";
	}
}
