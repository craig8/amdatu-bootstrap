/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.plugins.amdatu.swagger;

import java.io.File;

import org.amdatu.bootstrap.command.Command;
import org.amdatu.bootstrap.command.Description;
import org.amdatu.bootstrap.command.InstallResult;
import org.amdatu.bootstrap.command.Parameters;
import org.amdatu.bootstrap.command.Required;
import org.amdatu.bootstrap.command.RunConfig;
import org.amdatu.bootstrap.core.BootstrapPlugin;
import org.amdatu.bootstrap.services.Dependency;
import org.amdatu.bootstrap.services.DependencyBuilder;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;

@Component
public class SwaggerPlugin implements BootstrapPlugin{

	@ServiceDependency
	private volatile DependencyBuilder m_dependencies;
	

	interface RunParams extends Parameters{
		@Required
		@RunConfig
		@Description("Run configuration to add dependencies to")
		File bndRunFile();
	}
	
	@Command
	public InstallResult run(RunParams params) {
		return m_dependencies.addRunDependency(
				Dependency.fromStrings(
						"org.amdatu.web.rest.doc.swagger", 
						"org.amdatu.web.rest.doc.swagger.ui", 
						"org.amdatu.web.resourcehandler"), params.bndRunFile().toPath());
	}
	
	@Override
	public String getName() {
		return "swagger";
	}

}
