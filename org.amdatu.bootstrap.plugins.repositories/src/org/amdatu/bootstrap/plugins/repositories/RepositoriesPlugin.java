/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.plugins.repositories;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import org.amdatu.bootstrap.command.Command;
import org.amdatu.bootstrap.command.Description;
import org.amdatu.bootstrap.command.Parameters;
import org.amdatu.bootstrap.command.Required;
import org.amdatu.bootstrap.command.RunConfig;
import org.amdatu.bootstrap.command.Scope;
import org.amdatu.bootstrap.command.Select;
import org.amdatu.bootstrap.command.SelectOption;
import org.amdatu.bootstrap.core.BootstrapPlugin;
import org.amdatu.bootstrap.gradle.GradleService;
import org.amdatu.bootstrap.services.Navigator;
import org.amdatu.bootstrap.services.Prompt;
import org.amdatu.bootstrap.services.ResourceManager;
import org.amdatu.bootstrap.services.repositories.DependencyFilter;
import org.amdatu.bootstrap.services.repositories.DependencyType;
import org.amdatu.bootstrap.services.repositories.RepositoriesService;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.archive.ArchiveFormats;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;

import aQute.bnd.build.model.BndEditModel;
import aQute.bnd.build.model.clauses.HeaderClause;
import aQute.bnd.header.Attrs;
import aQute.bnd.properties.Document;


@Component
public class RepositoriesPlugin implements BootstrapPlugin{
	@ServiceDependency 
	private volatile RepositoriesService m_repositoriesService;
	
	@ServiceDependency
	private volatile Navigator m_navigator;
	
	@ServiceDependency 
	private volatile ResourceManager m_resourceManager;
	
	@ServiceDependency 
	private volatile GradleService m_gradle;
	
	interface MaterializeParams extends Parameters {
		@RunConfig
		File bndFile();
		
		File outputDir();
		
		@Select(value={
				@SelectOption(description="Runtime dependencies", value="RUN"), 
				@SelectOption(description="Build dependencies", value="BUILD"),
				@SelectOption(description="Run and build dependencies", value="ALL")})
		String dependenciesType();
		
		@Select(value={
				@SelectOption(description="No filter", value="NONE"), 
				@SelectOption(description="Include only non-workspace bundles", value="NON_WORKSPACE_ONLY"),
				@SelectOption(description="Include only workspace bundles", value="WORKSPACE_ONLY")})
		String dependenciesFilter();
	}
	
	@Command
	public void materialize(MaterializeParams args, Prompt prompt) {
		DependencyType dependencyType = DependencyType.valueOf(args.dependenciesType());
		DependencyFilter dependencyFilter = DependencyFilter.valueOf(args.dependenciesFilter());
		
		m_repositoriesService.materialize(args.bndFile(), args.outputDir().toPath(), dependencyType, dependencyFilter);
	}
	
	
	interface MaterializeWsParams extends Parameters {
		@Description("Git tag or SHA to checkout, build and use as source")
		@Required
		String gitTag();
		
		@Description("Path to directory to place resulting repository to. Must be empty and defaults to 'baselinerepo'")
		File outputDir();
		
		@Description("Configure the result repository as baseline repository in repositories.bnd")
		boolean setAsBaselineRepo();
		
		@Description("Enable baselining within Bndtools")
		boolean enableBaselining();
	}
	
	@Command(scope=Scope.WORKSPACE)
	public void materializeWorkspace(MaterializeWsParams params, Prompt prompt) throws Exception {
		FileRepositoryBuilder builder = new FileRepositoryBuilder();
		Repository repository = builder.setGitDir(m_navigator.getWorkspaceDir().resolve(".git").toFile())
				.readEnvironment() 
				.findGitDir() 
				.build();

		ArchiveFormats.registerAll();

		Path tempFile = Files.createTempFile(null, null);
		try (FileOutputStream fos = new FileOutputStream(tempFile.toFile())) {
			new Git(repository).archive()
					.setTree(repository.resolve(params.gitTag()))
					.setFormat("zip")
					.setOutputStream(fos)
					.call();
		}
		
		Path tempWsDir = Files.createTempDirectory("bootstrap-ws");
		
		m_resourceManager.unzipFile(tempFile.toFile(), tempWsDir.toFile());
		prompt.printf("Building from temporary workspace directory %s", tempWsDir);
		m_gradle.build(tempWsDir, prompt, "build");
		File outputDir = params.outputDir() != null ? params.outputDir() : m_navigator.getWorkspaceDir().resolve("baselinerepo").toFile();
		if(outputDir.exists() && outputDir.list().length > 0) {
			throw new IllegalArgumentException("Output directory is not empty!");
		}
		
		m_repositoriesService.materializeWorkspace(tempWsDir, outputDir);
		
		if(params.setAsBaselineRepo()) {
			BndEditModel editModel = new BndEditModel();
			Path reposFile = m_navigator.getWorkspaceDir().resolve("cnf/ext/repositories.bnd");
			editModel.loadFrom(reposFile.toFile());
			
			editModel.genericSet("-baselinerepo", "baseline");
			
			if(params.enableBaselining()) {
				editModel.genericSet("-baseline", "*");
			}
			

			List<HeaderClause> plugins = editModel.getPlugins();
			Attrs attrs = new Attrs();
			attrs.put("name", "baseline");
			attrs.put("locations", "file://" + outputDir.toPath().resolve("index.xml.gz").toString());
			
			HeaderClause header = new HeaderClause("aQute.bnd.deployer.repository.FixedIndexedRepo", attrs);
			plugins.add(header);
			
			editModel.setPlugins(plugins);
			
			Document document = new Document(new String(Files.readAllBytes(reposFile)));
			editModel.saveChangesTo(document);
			
			m_resourceManager.writeFile(reposFile, document.get().getBytes());
		}
		
	}

	@Override
	public String getName() {
		return "repositories";
	}
}
