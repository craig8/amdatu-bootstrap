package org.amdatu.bootstrap.services.repositories;

public enum DependencyFilter {
	WORKSPACE_ONLY, NON_WORKSPACE_ONLY, NONE
}
