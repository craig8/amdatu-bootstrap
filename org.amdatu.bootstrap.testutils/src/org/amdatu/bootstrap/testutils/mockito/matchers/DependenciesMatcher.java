/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.testutils.mockito.matchers;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

import org.amdatu.bootstrap.services.Dependency;
import org.mockito.ArgumentMatcher;

public class DependenciesMatcher extends ArgumentMatcher<Collection<Dependency>>{
	private final Set<String> m_expectedContains;
	
	public DependenciesMatcher(Set<String> expectedContains) {
		m_expectedContains = expectedContains;
	}
	
	@Override
	public boolean matches(Object argument) {
		@SuppressWarnings("unchecked")
		Collection<Dependency> deps = (Collection<Dependency>)argument;
		
		Set<String> bsns = deps.stream()
				.map(Dependency::getBsn)
				.collect(Collectors.toSet());
		
		return bsns.containsAll(m_expectedContains);
	}

}
