/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.plugins.baseline;

import static org.amdatu.bootstrap.java8.Java8.uncheck;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.amdatu.bootstrap.command.Command;
import org.amdatu.bootstrap.command.Description;
import org.amdatu.bootstrap.command.Parameters;
import org.amdatu.bootstrap.command.Required;
import org.amdatu.bootstrap.command.Scope;
import org.amdatu.bootstrap.core.BootstrapPlugin;
import org.amdatu.bootstrap.services.Navigator;
import org.amdatu.bootstrap.services.Prompt;
import org.amdatu.bootstrap.services.ResourceManager;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;

import aQute.bnd.build.Project;
import aQute.bnd.build.ProjectBuilder;
import aQute.bnd.build.Workspace;
import aQute.bnd.build.model.BndEditModel;
import aQute.bnd.differ.Baseline;
import aQute.bnd.differ.Baseline.Info;
import aQute.bnd.differ.DiffPluginImpl;
import aQute.bnd.osgi.Constants;
import aQute.bnd.osgi.Instructions;
import aQute.bnd.osgi.Jar;
import aQute.bnd.osgi.Processor;
import aQute.bnd.properties.Document;
import aQute.bnd.service.RepositoryPlugin;
import aQute.bnd.version.Version;

@Component
public class BaselineCommand implements BootstrapPlugin {

	@ServiceDependency
	private volatile Navigator m_navigator;
	
	@ServiceDependency
	private volatile ResourceManager m_resourceManager;
	
	@Command(scope=Scope.WORKSPACE)
	public void check(Prompt prompt) {
		try {
			prompt.println("Baselining running...");
			DiffPluginImpl differ = getDiffPlugin();
			
			Map<String, List<BaseLineError>> baseLineErrors = new HashMap<>();
	        List<File> generatedJars = getGeneratedJars();
	        
	        for (File jarFile : generatedJars) {
	            try (Jar jar = new Jar(jarFile)) {
	                String bsn = jar.getBsn();
	                Version version = new Version(jar.getVersion());
	                
                    Jar baseLineJar = getBaselineJar(bsn,version, prompt);
                    if (baseLineJar != null) {
	                    Processor bnd = new Processor();
	                    Baseline baseLine = new Baseline(bnd, differ);
	                    baseLine.baseline(jar, baseLineJar, null);
	                    List<BaseLineError> errors = new ArrayList<>();
	                    if (baseLine.getBundleInfo().mismatch) {
	                        BaseLineError err = new BaseLineError();
	                        err.bsn = bsn;
	                        err.newVersion = baseLine.getSuggestedVersion().toString();
	                        errors.add(err);
	                    } 
	                    for (Info i : baseLine.getPackageInfos()) {
	                        if (i.mismatch) {
	                            BaseLineError err = new BaseLineError();
	                            err.bsn = bsn;
	                            err.packageName = i.packageName;
	                            err.newVersion = baseLine.getSuggestedVersion().toString();
	                            errors.add(err);
	                        }
	                    }
	                    if (!errors.isEmpty()) {
	                        baseLineErrors.put(bsn, errors);
	                    }
		            } else {
		                prompt.println("No baseline for: " + bsn + " " + version);
		            }
                }
	        }

            Map<String, String> questions = new HashMap<>();

	        if (!baseLineErrors.isEmpty()) {
	            for (Entry<String, List<BaseLineError>> entry : baseLineErrors.entrySet()) {
	            	StringBuilder b = new StringBuilder();
	                for (BaseLineError e : entry.getValue()) {
	                	b.append(e.toString()).append(" ");
	                }
	                
	                questions.put(entry.getKey(), b.toString());
	            	
	            }

	            Map<String, Boolean> fixes = prompt.askMultipleBoolean("Fix version?", questions, false);  
	            Map<String, List<BaseLineError>> toFix = baseLineErrors.entrySet().stream().filter(e -> fixes.containsKey(e.getKey())).collect(Collectors.toMap(e-> e.getKey(), e -> e.getValue()));
	            
	            fixes.keySet().forEach(k -> fixBaseLineErrors(toFix));
	        } else {
	        	prompt.println("Baseline OK.");
	        }
	        

			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	interface ExplainParameters extends Parameters {
        @Description("The bsn of the bundle to explain baseline errors of")
        @Required
        String bsn();
    }
	
	@Command(scope=Scope.WORKSPACE)
    public void explain(Prompt prompt, ExplainParameters params) {
        try {
            prompt.println("Baselining explain running...");
            DiffPluginImpl differ = getDiffPlugin();
            
            List<File> generatedJars = getGeneratedJars();
            
            //Find the JAR file by bsn
            Jar jar = generatedJars.stream()
            	.map(f -> uncheck(()-> new Jar(f)))
            	.filter(j -> uncheck(() -> j.getBsn().equals(params.bsn())))
            	.findAny()
            	.orElseThrow(() -> new RuntimeException("Bundle with bsn " + params.bsn() + " was not found"));
            
            Version version = new Version(jar.getVersion());
            Jar baseLineJar = getBaselineJar(params.bsn(),version, prompt);
            if (baseLineJar != null) {
                prompt.println("Using " + version.toString() + " of the local file to compare to baseline version " + baseLineJar.getVersion());
                Processor bnd = new Processor();
                Baseline baseLine = new Baseline(bnd, differ);
                baseLine.baseline(jar, baseLineJar, null);
                boolean error = false;
                if (baseLine.getBundleInfo().mismatch) {
                   error = true;
                } 
                for (Info i : baseLine.getPackageInfos()) {
                    if (i.mismatch) {
                        error = true;
                    }
                }
                if (error) {
                    // Create folders to expand file into
                    String tempdir = System.getProperty("java.io.tmpdir");
                    File currentExpand = new File(tempdir + File.separator + "current" + params.bsn());
                    File repoExpand = new File(tempdir + File.separator + "repo" + params.bsn());
                    currentExpand.mkdirs();
                    repoExpand.mkdirs();
                    
                    jar.expand(currentExpand);
                    baseLineJar.expand(repoExpand);
                    
                    getDiff(currentExpand, repoExpand, prompt);
                    
                    // Cleanup
                    deleteFolder(currentExpand);
                    deleteFolder(repoExpand);
                }
            } else {
                prompt.println("No baseline for: " + params.bsn() + " " + version);
            }
        } catch(Exception ex) {
        	throw new RuntimeException(ex);
        }
    }
	
	private DiffPluginImpl getDiffPlugin(){
		DiffPluginImpl differ = new DiffPluginImpl();
		String diffignore = m_navigator.getCurrentWorkspace().get(Constants.DIFFIGNORE);
		differ.setIgnore(diffignore);
		return differ;
	}
	
    private void executeProgram(String program, Prompt prompt) {
        executeProgram(program, null, prompt);
    }

    private void executeProgram(String program, File output, Prompt prompt) {
        try {
            String line;
            ProcessBuilder builder = new ProcessBuilder(program.split(" "));
            if (output != null) {
                builder.redirectOutput(output);
                builder.redirectError(output);
            }
            Process p = builder.start();
            if (output == null) {
                BufferedReader input = new BufferedReader (new InputStreamReader(p.getInputStream()));
                BufferedReader error = new BufferedReader (new InputStreamReader(p.getErrorStream()));
                while ((line = input.readLine()) != null) {
                    prompt.println(line);
                }
                while ((line = error.readLine()) != null) {
                    prompt.println("[ERROR] " + line);
                }
                input.close();
                error.close();
            }
            
            p.waitFor();
        }
        catch (Exception err) {
            prompt.println("Failed to start: " + program);
        }
    }
	
    private void deleteFolder(File f) throws IOException {
        if (f.isDirectory()) {
            for (File c : f.listFiles())
                deleteFolder(c);
        }
        if (!f.delete()) {
            throw new FileNotFoundException("Failed to delete file: " + f);
        }
    }

    private void getDiff(File dirA, File dirB, Prompt prompt) throws IOException {
        File[] fileList1 = dirA.listFiles();
        File[] fileList2 = dirB.listFiles();
        Arrays.sort(fileList1);
        Arrays.sort(fileList2);
        HashMap<String, File> map1;
        if (fileList1.length < fileList2.length) {
            map1 = new HashMap<String, File>();
            for (int i = 0; i < fileList1.length; i++) {
                map1.put(fileList1[i].getName(), fileList1[i]);
            }

            compareNow(fileList2, map1, prompt);
        }
        else {
            map1 = new HashMap<String, File>();
            for (int i = 0; i < fileList2.length; i++) {
                map1.put(fileList2[i].getName(), fileList2[i]);
            }
            compareNow(fileList1, map1, prompt);
        }
    }

    private void compareNow(File[] fileArr, HashMap<String, File> map, Prompt prompt) throws IOException {
    	Instructions ignoreInstructions = null;
    	String diffignore = m_navigator.getCurrentWorkspace().get(Constants.DIFFIGNORE);
    	if (diffignore != null){
    		aQute.bnd.header.Parameters ignoreParams = new aQute.bnd.header.Parameters(diffignore);
    		ignoreInstructions = new Instructions(ignoreParams);
    	}

        for (int i = 0; i < fileArr.length; i++) {
            String fName = fileArr[i].getName();
            File fComp = map.remove(fName);
            if (fComp != null) {
            	if (fComp.isDirectory()) {
                    getDiff(fileArr[i], fComp, prompt);
                }
                else {
                	if (ignoreInstructions != null ){
    					Pattern pattern = Pattern.compile(".*(repo|current).*?/(.*)");
    					Matcher matcher = pattern.matcher(fComp.getAbsolutePath());
    					if (matcher.matches() && ignoreInstructions.matches(matcher.group(2))){
    						System.out.println("ignore " + fName + " " + matcher.group(2) );
    						continue;
    					}
                	}
                	
                	String cSum1 = checksum(fileArr[i]);
                    String cSum2 = checksum(fComp);
                    if (!cSum1.equals(cSum2)) {
                        prompt.println(fileArr[i].getName() + "\t\t" + "different:");
                        if (fileArr[i].getName().endsWith(".class")) {
                            // We can't just diff 2 class files as they are binary so disasseble them
                            executeProgram("javap -v " + fileArr[i], new File(fileArr[i] + ".disasseble"), prompt);
                            executeProgram("javap -v " + fComp, new File(fComp + ".disasseble"), prompt);

                            // Now compare the dissabbled stuff
                            executeProgram("diff " + fileArr[i] + ".disasseble " + fComp +".disasseble", prompt);
                        }
                        else {
                            executeProgram("diff " + fileArr[i] + " " + fComp, prompt);
                        }
                    }
                }
            }
            else {
                if (fileArr[i].isDirectory()) {
                    traverseDirectory(fileArr[i], prompt);
                }
                else {
                    prompt.println(fileArr[i].getName() + "\t\t" + "only in " + fileArr[i].getParent());
                }
            }
        }
        Set<String> set = map.keySet();
        Iterator<String> it = set.iterator();
        while (it.hasNext()) {
            String n = it.next();
            File fileFrmMap = map.get(n);
            map.remove(n);
            if (fileFrmMap.isDirectory()) {
                traverseDirectory(fileFrmMap, prompt);
            }
            else {
                prompt.println(fileFrmMap.getName() + "\t\t" + "only in " + fileFrmMap.getParent());
            }
        }
    }

    private void traverseDirectory(File dir, Prompt prompt) {
        File[] list = dir.listFiles();
        for (int k = 0; k < list.length; k++) {
            if (list[k].isDirectory()) {
                traverseDirectory(list[k], prompt);
            }
            else {
               prompt.println(list[k].getName() + "\t\t" + "only in " + list[k].getParent());
            }
        }
    }

    private String checksum(File file) {
        try (InputStream fin = new FileInputStream(file)) {
            java.security.MessageDigest md5er = MessageDigest.getInstance("MD5");
            byte[] buffer = new byte[1024];
            int read;
            do {
                read = fin.read(buffer);
                if (read > 0)
                    md5er.update(buffer, 0, read);
            }
            while (read != -1);
            fin.close();
            byte[] digest = md5er.digest();
            if (digest == null) {
                return null;
            }
            String strDigest = "0x";
            for (int i = 0; i < digest.length; i++) {
                strDigest += Integer.toString((digest[i] & 0xff) + 0x100, 16).substring(1).toUpperCase();
            }
            return strDigest;
        }
        catch (Exception e) {
            // Ignore
            return null;
        }
    }
	
	
	private void fixBaseLineErrors(Map<String, List<BaseLineError>> baseLineErrors) {
        List<File> generatedJars;
		try {
			generatedJars = getGeneratedJars();

			for (File jarFile : generatedJars) {
				try (Jar jar = new Jar(jarFile)) {
					String bsn = jar.getBsn();
					if (baseLineErrors.containsKey(bsn)) {
						for (BaseLineError err : baseLineErrors.get(bsn)) {
							if (err.packageName == null) {
								fixProject(jarFile, bsn, err);
							} else {
								fixPackage(jarFile, bsn, err);
							}
						}
					}
				}
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
    }
	
	
	private void fixPackage(File jarFile, String bsn, BaseLineError err) throws IOException {
	        File projectFolder = jarFile.getParentFile().getParentFile();
	        List<File> packageFiles = m_resourceManager.findFiles(projectFolder, "packageinfo");
	    for (File f : packageFiles) {
	        if (f.getAbsolutePath().replace(File.separatorChar, '.').contains(err.packageName)) {
	            try (PrintWriter out = new PrintWriter(f)){
	                out.print("version " + err.newVersion + "\n");
	            }
	        }
	    }
	}

	private void fixProject(File jarFile, String bsn, BaseLineError err) {
	    File projectFolder = jarFile.getParentFile().getParentFile();
        try {
            Project project = m_navigator.getCurrentWorkspace().getProject(projectFolder.getName());
            
            File bndFile = null;
            if (project.get(Constants.SUB) == null && project.getBuilder(null).getBsn().equals(bsn)){
            	bndFile = project.getPropertiesFile();
            }else{
            	ProjectBuilder subBuilder = project.getSubBuilder(bsn);
            	bndFile = subBuilder.getPropertiesFile();
            }

            BndEditModel model = new BndEditModel();
            model.loadFrom(bndFile);
   
            model.setBundleVersion(err.newVersion);
   
            Document document = new Document(new String(Files.readAllBytes(bndFile.toPath())));
            model.saveChangesTo(document);
   
            String documentContents = document.get();
            m_resourceManager.writeFile(bndFile.toPath(), documentContents.getBytes());
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
	 
	}

	private Jar getBaselineJar(String bsn, Version version, Prompt prompt) {
		try {
			List<RepositoryPlugin> repositories = getBaselineRepos(prompt);
			SortedMap<Version, RepositoryPlugin> versions = new TreeMap<>();
			for (RepositoryPlugin repo: repositories){
				SortedSet<Version> versions2 = repo.versions(bsn);
				for (Version v : versions2){
					versions.putIfAbsent(v, repo);
				}
			}
			
			if (versions.isEmpty()){
				return null;
			}
			
			RepositoryPlugin repositoryPlugin = versions.get(versions.lastKey());
			
			File file = repositoryPlugin.get(bsn, versions.lastKey(), new HashMap<String, String>() );
			if (file == null){
				System.err.println("Failed to get " + bsn + " with version " + versions.lastKey()
	                    + " skipping baseline of this file!");
				return null;
			}
		
			return new Jar(file);
		
		} catch (Exception e) {
			System.err.println("Failed to get baseline jar for " + bsn  + " skipping baseline of this file! " + e.getMessage());
		}
		return null;
	}
	
	private List<RepositoryPlugin> getBaselineRepos(Prompt prompt) {
		Workspace workspace = m_navigator.getCurrentWorkspace();

		String baselineRepo = workspace.get(Constants.BASELINEREPO);
		if (baselineRepo != null) {
			for (RepositoryPlugin p : workspace.getRepositories()) {
				if (p.getName().equals(baselineRepo)) {
					return Arrays.asList(p);
				}
			}
			prompt.println("WARNING! Baseline repo " + baselineRepo + " not found, using all repositories for baselining");
		}

		return workspace.getRepositories();
	}

	private List<File> getGeneratedJars() throws Exception {
        List<File> result = new ArrayList<>();
        
        Project project = m_navigator.getCurrentProject();
        if (project != null) {
            result.addAll(addGeneratedJarsFromProject(project));
        } else {
            Collection<Project> projects = m_navigator.getCurrentWorkspace().getAllProjects();
			for (Project p : projects) {
				result.addAll(addGeneratedJarsFromProject(p));
			}
        }
        return result;
    }
    
    private List<File> addGeneratedJarsFromProject(Project project) {
    	File genFolder = project.getTargetDir();
        
        if (genFolder.exists() && genFolder.isDirectory()) {
            File[] jars = genFolder.listFiles(new FileFilter() {
                @Override
                public boolean accept(File pathname) {
                    return pathname.getName().endsWith(".jar");
                }
            });
            return Arrays.asList(jars);
        }
        return Collections.emptyList();
    }
    
    /** A data holder class for the baseline errors. */
    private class BaseLineError {
        String bsn;
        String packageName;
        String newVersion;
        
        @Override
        public String toString() {
            if (packageName != null) {
                return "In " + bsn + " package: " + packageName + " should be " + newVersion;
            }
            return bsn + " should be " + newVersion;
        }
    }


	@Override
	public String getName() {
		return "baseline";
	}
}
