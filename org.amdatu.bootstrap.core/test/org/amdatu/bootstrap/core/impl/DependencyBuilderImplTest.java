/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.core.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.amdatu.bootstrap.command.InstallResult;
import org.amdatu.bootstrap.services.Dependency;
import org.amdatu.bootstrap.services.DependencyBuilder;
import org.amdatu.bootstrap.services.Navigator;
import org.amdatu.bootstrap.services.ResourceManager;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.osgi.service.event.EventAdmin;

import aQute.bnd.build.Project;
import aQute.bnd.build.Workspace;
import aQute.bnd.build.model.BndEditModel;

@RunWith(MockitoJUnitRunner.class)
public class DependencyBuilderImplTest {

	@Spy @InjectMocks 
	private	DependencyBuilder m_dependencyBuilder = new DependencyBuilderImpl();
	
	@Mock
	private Navigator m_navigator;
	
	@Mock
	private ResourceManager m_resourceManager;
	
	@Mock
	private EventAdmin m_eventAdmin;
	
	private Workspace m_workspace;
	private Project m_project;
	
	@Before
	public void setUp() throws Exception {
		URL resource = this.getClass().getClassLoader().getResource("ws");
		
		m_workspace = new Workspace(Paths.get(resource.toURI()).toFile());
		m_project = new Project(m_workspace, new File("ws/p1"));
		
		when(m_navigator.getCurrentProject()).thenReturn(m_project);
	}

	@Test
	public void testAddDependencyWithoutVersion() throws Exception{
		Path path = setupBndFile();
		
		m_dependencyBuilder.addDependency("org.mytest");
		
		BndEditModel model = getEditModel(path);
		
		assertEquals(1, model.getBuildPath().size());
		String versionRange = model.getBuildPath().get(0).getVersionRange();
		assertNull(versionRange);
	}
	
	@Test
	public void testAddDependencyWithVersion() throws Exception {
		Path path = setupBndFile();
		
		m_dependencyBuilder.addDependency("org.mytest", "1.0.0");
		
		BndEditModel model = getEditModel(path);
		
		assertEquals(1, model.getBuildPath().size());
		String versionRange = model.getBuildPath().get(0).getVersionRange();
		assertEquals("1.0.0", versionRange);
	}
	
	@Test
	public void testAddDependencyWithVersionRange() throws Exception {
		Path path = setupBndFile();
		m_dependencyBuilder.addDependency("org.mytest", "[1.0.0,2.0.0)");
		
		BndEditModel model = getEditModel(path);
		
		assertEquals(1, model.getBuildPath().size());
		String versionRange = model.getBuildPath().get(0).getVersionRange();
		assertEquals("[1.0.0,2.0.0)", versionRange);
	}
	
	@Test
	public void testAddDependencyTypeNoVersion() throws Exception {
		Path path = setupBndFile();
		m_dependencyBuilder.addDependency(new Dependency("org.mytest"));
		
		BndEditModel model = getEditModel(path);
		
		assertEquals(1, model.getBuildPath().size());
		String versionRange = model.getBuildPath().get(0).getVersionRange();
		assertNull(versionRange);
	}
	
	@Test
	public void testAddDependencyTypeWithVersion() throws Exception {
		Path path = setupBndFile();
		m_dependencyBuilder.addDependency(new Dependency("org.mytest", "1.0.0"));
		
		BndEditModel model = getEditModel(path);
		
		assertEquals(1, model.getBuildPath().size());
		String versionRange = model.getBuildPath().get(0).getVersionRange();
		assertEquals("1.0.0", versionRange);
	}
	
	@Test
	public void testAddDependencyTypeWithVersionRange() throws Exception {
		Path path = setupBndFile();
		m_dependencyBuilder.addDependency(new Dependency("org.mytest", "[1.0.0,2.0.0)"));
		
		BndEditModel model = getEditModel(path);
		
		assertEquals(1, model.getBuildPath().size());
		String versionRange = model.getBuildPath().get(0).getVersionRange();
		assertEquals("[1.0.0,2.0.0)", versionRange);
	}
	
	@Test
	public void testAddDependencies() throws Exception {
		Path path = setupBndFile();
		m_dependencyBuilder.addDependencies(Dependency.fromStrings("org.mytest;version=1.0.0", "org.myothertest;version='[1.0.0,2.0.0)'"));
		
		verify(m_resourceManager, times(2)).writeFile(Mockito.eq(path), Mockito.any(byte[].class));
	}
	
	

	/**
	 * Run dependencies
	 */
	@Test
	public void testAddRunDependencyWithoutVersion() throws Exception{
		Path path = setupBndFile();
		
		m_dependencyBuilder.addRunDependency("org.mytest",path);
		
		BndEditModel model = getEditModel(path);
		
		assertEquals(1, model.getRunBundles().size());
		String versionRange = model.getRunBundles().get(0).getVersionRange();
		assertNull(versionRange);
	}
	
	@Test
	public void testAddRunDependencyWithVersion() throws Exception {
		Path path = setupBndFile();
		
		m_dependencyBuilder.addRunDependency("org.mytest", "1.0.0", path);
		
		BndEditModel model = getEditModel(path);
		
		assertEquals(1, model.getRunBundles().size());
		String versionRange = model.getRunBundles().get(0).getVersionRange();
		assertEquals("[1.0.0,1.0.0]", versionRange);
	}
	
	@Test
	public void testAddRunDependencyWithVersionRange() throws Exception {
		Path path = setupBndFile();
		m_dependencyBuilder.addRunDependency("org.mytest", "[1.0.0,2.0.0)", path);
		
		BndEditModel model = getEditModel(path);
		
		assertEquals(1, model.getRunBundles().size());
		String versionRange = model.getRunBundles().get(0).getVersionRange();
		assertEquals("[1.0.0,2.0.0)", versionRange);
	}
	
	@Test
	public void testAddRunDependencyTypeNoVersion() throws Exception {
		Path path = setupBndFile();
		m_dependencyBuilder.addRunDependency(new Dependency("org.mytest"), path);
		
		BndEditModel model = getEditModel(path);
		
		assertEquals(1, model.getRunBundles().size());
		String versionRange = model.getRunBundles().get(0).getVersionRange();
		assertNull(versionRange);
	}
	
	@Test
	public void testAddRunDependencyTypeWithVersion() throws Exception {
		Path path = setupBndFile();
		m_dependencyBuilder.addRunDependency(new Dependency("org.mytest", "1.0.0"), path);
		
		BndEditModel model = getEditModel(path);
		
		assertEquals(1, model.getRunBundles().size());
		String versionRange = model.getRunBundles().get(0).getVersionRange();
		assertEquals("[1.0.0,1.0.0]", versionRange);
	}
	
	@Test
	public void testAddRunDependencyTypeWithVersionRange() throws Exception {
		Path path = setupBndFile();
		m_dependencyBuilder.addRunDependency(new Dependency("org.mytest", "[1.0.0,2.0.0)"),path);
		
		BndEditModel model = getEditModel(path);
		
		assertEquals(1, model.getRunBundles().size());
		String versionRange = model.getRunBundles().get(0).getVersionRange();
		assertEquals("[1.0.0,2.0.0)", versionRange);
	}
	
	@Test
	public void testAddRunDependencies() throws Exception {
		Path path = setupBndFile();
		m_dependencyBuilder.addRunDependency(Dependency.fromStrings("org.mytest;version=1.0.0", "org.myothertest;version='[1.0.0,2.0.0)'"),path);
		
		BndEditModel model = getEditModel(path);
		assertEquals(2, model.getRunBundles().size());
	}	
	
	@Test
	public void testIntallResultForAdd() throws Exception {
		setupBndFile();
		InstallResult addDependency = m_dependencyBuilder.addDependency("org.mytest");
		assertEquals(1, addDependency.getInstalledDependencies().size());
	}
	
	
	private Path setupBndFile() throws URISyntaxException {
		URL bndresource = this.getClass().getClassLoader().getResource("ws/p1/bnd.bnd");
		
		Path path = Paths.get(bndresource.toURI());
		when(m_navigator.getBndFile()).thenReturn(path);
		return path;
	}
	

	private BndEditModel getEditModel(Path path) throws IOException {
		ArgumentCaptor<byte[]> captor = ArgumentCaptor.forClass(byte[].class);
		verify(m_resourceManager).writeFile(Mockito.eq(path), captor.capture());
		
		BndEditModel model = new BndEditModel();
		model.loadFrom(new ByteArrayInputStream(captor.getValue()));
		return model;
	}
}
