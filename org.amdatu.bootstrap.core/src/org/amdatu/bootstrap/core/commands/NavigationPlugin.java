/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.core.commands;

import static org.amdatu.bootstrap.java8.Java8.uncheck;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.amdatu.bootstrap.command.Command;
import org.amdatu.bootstrap.command.Description;
import org.amdatu.bootstrap.command.Parameters;
import org.amdatu.bootstrap.core.BootstrapPlugin;
import org.amdatu.bootstrap.services.Navigator;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.osgi.service.event.EventAdmin;

@Component
public class NavigationPlugin implements BootstrapPlugin {

	@ServiceDependency
	private volatile Navigator m_navigator;

	@ServiceDependency
	private volatile EventAdmin m_eventAdmin;
	
	interface CdParameters extends Parameters {
		String dir();
	}
	
	@Command
	public File cd(CdParameters params) {
		Path currentDir = m_navigator.getCurrentDir();
		Path newDir;
		
		if (params.dir().equals("..")) {
			newDir = currentDir.getParent();
		}
		else if (params.dir().equals("-")) {
			newDir = m_navigator.getPreviousDir();
		}
		else if (params.dir().equals("~")) {
			newDir = m_navigator.getHomeDir();
		}
		else if (params.dir().startsWith("~" + File.separator)) {
	        newDir = m_navigator.getHomeDir().resolve(params.dir().substring(2));
		}
		else {
		    newDir = currentDir.resolve(params.dir());
		}
		
		if (newDir.toFile().exists() && newDir.toFile().isDirectory()) {
			m_navigator.changeDir(newDir);
			return newDir.toFile();
		}
		else {
			throw new IllegalArgumentException("Invalid directory " + newDir);
		}
	}

	@Command
	public File pwd() {
		return m_navigator.getCurrentDir().toFile();
	}
	
	interface LsParameters extends Parameters {
		
		@Description("List of files to traverse, if not set use the default directory")
		List<File> arguments();

		@Description("Recurse into directories")
		boolean recursive();

		@Description("Only return files")
		boolean filesOnly();

		@Description("Show all files (normally skips files starting with '.')")
		boolean all();

	}

	@Command
	@Description("Lists files in the given directories")
	public List<File> ls(LsParameters lsp) {
		try {
			List<File> args = lsp.arguments();
			if(args == null) {
				args = new ArrayList<>();
			}
			if (args.isEmpty()){
				args.add(m_navigator.getCurrentDir().toFile());
			}
			
			Stream<Path> stream = args.stream().map(File::toPath);

			int depth = lsp.recursive() ? Integer.MAX_VALUE : 1;
			stream = stream.flatMap(path -> uncheck(() -> Files.walk(path,
					depth)));

			if (!lsp.all())
				stream = stream.filter(p -> {
					return !p.getFileName().toString().startsWith(".");
				});
			
			Stream<File> files = stream.map(Path::toFile);

			if (lsp.filesOnly())
				files = files.filter(File::isFile);

			List<File> collect = files.collect(Collectors.toList());
			return collect;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	interface MkdirParams extends Parameters {
		@Description("List of dirs to create")
		List<File> arguments();
	}
	
	@Command
	@Description("Creates one or more directories")
	public void mkdir(MkdirParams params) {
		params.arguments().stream()
			.map(File::toPath)
			.map(p -> m_navigator.getCurrentDir().resolve(p))
			.map(Path::toFile)
			.forEach(File::mkdirs);
	}

	@Override
	public String getName() {
		return "navigation";
	}
}
