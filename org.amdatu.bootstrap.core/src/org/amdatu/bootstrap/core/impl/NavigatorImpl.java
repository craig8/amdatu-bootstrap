/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.core.impl;

import static org.amdatu.bootstrap.java8.Java8.uncheck;

import java.io.File;
import java.io.FileFilter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.StandardOpenOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.amdatu.bootstrap.command.Scope;
import org.amdatu.bootstrap.core.EnumValue;
import org.amdatu.bootstrap.exceptions.InvalidProjectException;
import org.amdatu.bootstrap.exceptions.InvalidWorkspaceException;
import org.amdatu.bootstrap.services.Navigator;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.Inject;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.apache.felix.dm.annotation.api.Start;
import org.osgi.framework.BundleContext;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventAdmin;

import aQute.bnd.build.Project;
import aQute.bnd.build.Workspace;
import aQute.bnd.osgi.Builder;

@Component
public class NavigatorImpl implements Navigator {
	
	@ServiceDependency
	private volatile EventAdmin m_eventAdmin;
	
	@Inject
	private volatile BundleContext m_bundleContext;

	private Path m_workspaceDir;
	private Workspace m_workspace;
	private Project m_project;
	private Path m_projectDir;
	private Path m_currentDir;
	private Path m_previousDir;

	@Start
	public void start() {
		Path lastLocationPath = m_bundleContext.getDataFile("lastlocation.txt").toPath();
		try {
			String lastKnownDir = new String(Files.readAllBytes(lastLocationPath));
			changeDir(Paths.get(lastKnownDir));
		} catch (IOException e) {
		}
	}

	@Override
	public void changeDir(Path newDir) {
		m_previousDir = m_currentDir;
		m_currentDir = newDir;
		Path lastLocationPath = m_bundleContext.getDataFile("lastlocation.txt").toPath();
		try {
			Files.write(lastLocationPath, m_currentDir.toString().getBytes());
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

		if (m_workspaceDir != null && !m_currentDir.startsWith(m_workspaceDir)) {
			m_workspace = null;
			m_project = null;
			m_projectDir = null;
			m_workspaceDir = null;

			sendEvent("org/amdatu/bootstrap/core/WORKSPACE_LEFT");
		}

		if (m_projectDir != null && !m_currentDir.startsWith(m_projectDir)) {
			m_project = null;
			m_projectDir = null;

			sendEvent("org/amdatu/bootstrap/core/PROJECT_LEFT");
		}
		
		

		try {
			for (Path path : Files.newDirectoryStream(m_currentDir)) {
				File file = path.toFile();
				
				if (!m_currentDir.equals(m_workspaceDir) && file.isDirectory() && file.getName().equals("cnf")) {
					changeWorkspace(m_currentDir);
					
					List<String> workspaces = readRecentWorkspaces();
					
					workspaces.remove(m_currentDir.toString());
					
					workspaces.add(0, m_currentDir.toString());
					
					if(workspaces.size() > 5) {
						workspaces.remove(5);
					}
					
					Files.write(getLastWorkspacesLog().toPath(), workspaces, StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
					
					break;
				} else if (file.isFile() && file.getName().endsWith(".bnd")) {
					changeProject(m_currentDir);
					break;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		Map<String, Object> props = new HashMap<>();
		props.put("dir", newDir);
		sendEvent(Navigator.CHANGEDIR_TOPIC, props);
	}

	private List<String> readRecentWorkspaces() throws IOException {
		File workspaceHistoryDataFile = getLastWorkspacesLog();
		List<String> workspaces;
		if(workspaceHistoryDataFile.exists()) {
			workspaces = Files.readAllLines(workspaceHistoryDataFile.toPath());
		} else {
			workspaces = new ArrayList<>();
		}
		return workspaces;
	}

	private File getLastWorkspacesLog() {
		return m_bundleContext.getDataFile("lastworkspaces.txt");
	}

	private void sendEvent(String topicName) {
		Map<String, Object> props = new HashMap<>();
		sendEvent(topicName, props);
	}
	
	private void sendEvent(String topicName, Map<String, Object> properties) {
		Event event = new Event(topicName, properties);
		m_eventAdmin.sendEvent(event);
	}
	
	@Override
	public Workspace getCurrentWorkspace() {
		return m_workspace;
	}

	@Override
	public Path getWorkspaceDir() {
		return m_workspaceDir;
	}

	@Override
	public Project getCurrentProject() {
		return m_project;
	}

	@Override
	public Path getProjectDir() {
		return m_projectDir;
	}

	@Override
	public Path getCurrentDir() {
		if (m_currentDir == null) {
			m_currentDir = getHomeDir();
		}

		return m_currentDir;
	}



	@Override
    public List<Path> getBndRunFiles() {
	    File[] listFiles = m_currentDir.toFile().listFiles(new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                return pathname.isFile() && pathname.toString().toLowerCase().endsWith(".bndrun");
            }
        });
	    
	    List<Path> result = new ArrayList<>();

	    for (File f : listFiles) {
	        result.add(f.toPath());
	    }
	    
        return result;
    }
	
	public List<Project> getProjectsInWorkspace() {
		try {
			return Files.list(m_workspaceDir)
				.filter(p -> p.resolve("bnd.bnd").toFile().exists())
				.map(Path::toFile)
				.map(f-> uncheck(() -> Workspace.getProject(f)))
				.collect(Collectors.toList());
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

    private void changeWorkspace(Path workspaceDir) {
		try {
			m_workspace = new Workspace(workspaceDir.toFile());
			m_workspaceDir = workspaceDir;

			sendEvent("org/amdatu/bootstrap/core/WORKSPACE_CHANGED");
		} catch (Exception e) {
			throw new InvalidWorkspaceException(e);
		}
	}

	private void changeProject(Path projectDir) {
		try {
			if (!projectDir.getParent().equals(m_workspaceDir)) {
				changeWorkspace(projectDir.getParent());
			}

			m_projectDir = projectDir;

			m_project = Workspace.getProject(projectDir.toFile());

			sendEvent("org/amdatu/bootstrap/core/PROJECT_CHANGED");
		} catch (Exception e) {
			throw new InvalidProjectException(e);
		}
	}

	@Override
	public List<File> listProjectBndFiles() {
		return Arrays.asList(m_projectDir.toFile().listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.endsWith(".bnd");
			}
		}));
	}

	@Override
	public List<Path> findWorkspaceRunConfigs() {
		try {
			RunConfigVisitor visitor = new RunConfigVisitor();
			Files.walkFileTree(m_workspaceDir, visitor);
			return visitor.getRunConfigsFound();
		} catch (IOException ex) {
			throw new RuntimeException(ex);
		}
	}

    class RunConfigVisitor extends SimpleFileVisitor<Path> {
		private final PathMatcher matcher = FileSystems.getDefault().getPathMatcher("glob:/**/*.bndrun");
		private final PathMatcher bndMatcher = FileSystems.getDefault().getPathMatcher("glob:/**/*.bnd");
		private final List<Path> runConfigsFound = new ArrayList<>();

		@Override
		public FileVisitResult visitFile(Path file, BasicFileAttributes attr) {

			if (matcher.matches(file)) {
				runConfigsFound.add(file);
			} else
				try {
					if(isIntegrationTestBnd(file)) {
						runConfigsFound.add(file);
					}
				} catch (IOException e) {
					//This should never throw an error, if it does something is broken.
					throw new RuntimeException(e);
				}
			
			return FileVisitResult.CONTINUE;
		}

		private boolean isIntegrationTestBnd(Path file) throws IOException {
			return bndMatcher.matches(file) && new String(Files.readAllBytes(file)).contains("Test-Cases");
		}

		public List<Path> getRunConfigsFound() {
			return runConfigsFound;
		}

	}
    
	@Override
	public Path getHomeDir() {
		return Paths.get(System.getProperty("user.home"));
	}
	
	@Override
	public Path getPreviousDir() {
		return m_previousDir != null ? m_previousDir : getHomeDir();
	}
	
	@Override
	public Path getBndFile() {
		if (m_projectDir == null) {
			throw new IllegalStateException("Not in a project");
		}

		return m_projectDir.resolve("bnd.bnd");
	}

	@Override
	public Scope getCurrentScope() {
		if(m_project != null) {
			return Scope.PROJECT;
		} else if(m_workspace != null) {
			return Scope.WORKSPACE;
		} else {
			return Scope.GLOBAL;
		}
	}

	@Override
	public List<EnumValue> findWorkspaceBundles() {
		return Arrays.stream(m_workspaceDir.toFile().listFiles(f -> f.isDirectory()))
			.map(f -> uncheck(() -> Workspace.getProject(f)))
			.filter(p -> p != null)
			.flatMap(p -> uncheck(() -> p.getSubBuilders().stream()))
			.map(s -> s.getBsn())
			.map(s -> new EnumValue(s, s))
			.collect(Collectors.toList());
	}
	
	public List<EnumValue> findProjectBndFiles() {
		try {
			Collection<? extends Builder> subBuilders = getCurrentProject().getSubBuilders();
			if(subBuilders.size() == 1) {
				File file = getCurrentProject().getBase().toPath().resolve("bnd.bnd").toFile();
				return Arrays.asList(new EnumValue(file.getName(), file.getName()));
			}
			
			return subBuilders.stream()
					.map(Builder::getPropertiesFile)
					.map(File::getName)
					.map(f -> new EnumValue(f,f))
					.collect(Collectors.toList());
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public List<String> getRecentWorkspaces() {
		try {
			return readRecentWorkspaces();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}
