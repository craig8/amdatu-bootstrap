/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.core.impl;

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.amdatu.bootstrap.command.Command;
import org.amdatu.bootstrap.command.DescribedEnum;
import org.amdatu.bootstrap.command.Description;
import org.amdatu.bootstrap.command.DynamicSelect;
import org.amdatu.bootstrap.command.ProjectBndFile;
import org.amdatu.bootstrap.command.WorkspaceBundleSelect;
import org.amdatu.bootstrap.command.Parameters;
import org.amdatu.bootstrap.command.Required;
import org.amdatu.bootstrap.command.RunConfig;
import org.amdatu.bootstrap.command.Scope;
import org.amdatu.bootstrap.command.Select;
import org.amdatu.bootstrap.command.StoreValue;
import org.amdatu.bootstrap.core.CommandArgumentDescription;
import org.amdatu.bootstrap.core.CommandDescription;
import org.amdatu.bootstrap.core.EnumValue;
import org.amdatu.bootstrap.core.PluginDescription;
import org.amdatu.bootstrap.services.Prompt;
import aQute.bnd.annotation.ProviderType;

@ProviderType
public class PluginDescriptionBuilder {
	private String m_name;
	private List<CommandDescriptionBuilder> m_commands = new ArrayList<>(); 

	public PluginDescriptionBuilder(String name) {
		m_name = name;
	}
	
	public CommandDescriptionBuilder command(Method commandMethod) {
		CommandDescriptionBuilder commandBuilder = new CommandDescriptionBuilder(this, commandMethod);
		m_commands.add(commandBuilder);
		return commandBuilder;
	}
	
	public PluginDescription build() {
		List<CommandDescription> commands = m_commands.stream().map(CommandDescriptionBuilder::build).collect(Collectors.toList());
		return new PluginDescription(m_name, commands);
	}
	
	public static class CommandDescriptionBuilder {
		private PluginDescriptionBuilder m_pluginDescriptionBuilder;
		private String m_name;
		private String m_description;
		private List<CommandArgumentDescription> m_args = new ArrayList<>();
		private Scope m_scope;
		
		public CommandDescriptionBuilder(PluginDescriptionBuilder pluginDescriptionBuilder, Method commandMethod) {
			m_pluginDescriptionBuilder = pluginDescriptionBuilder;
			m_name = commandMethod.getName();
			
			if(commandMethod.isAnnotationPresent(Command.class) && commandMethod.getAnnotation(Command.class).scope() != null) {
				m_scope = commandMethod.getAnnotation(Command.class).scope();
			} else {
				m_scope = Scope.GLOBAL;
			}
			
			Arrays.stream(commandMethod.getParameters()).forEach(this::describeCommandParameters);
		}

		private void describeCommandParameters(Parameter arg) {
			if(Parameters.class.isAssignableFrom(arg.getType())) {
				Arrays.stream(arg.getType().getMethods()).forEach(m -> {
					Description description = m.getAnnotation(Description.class);
					String descriptionString = description != null ? description.value() : m.getName();
					
					if(m.getReturnType().isEnum()) {
						Object[] enumOptons = m.getReturnType().getEnumConstants();
						List<EnumValue> options = Arrays.stream(enumOptons).map(this::createEnumValue).collect(Collectors.toList());
						m_args.add(new CommandArgumentDescription(m.getName(), descriptionString, m.isAnnotationPresent(Required.class), m.isAnnotationPresent(Required.class), options));
					} else if(m.isAnnotationPresent(Select.class)) {
						
						List<EnumValue> options = Arrays.stream(m.getAnnotation(Select.class).value())
								.map(o -> new EnumValue(o.value(), o.description()))
								.collect(Collectors.toList());
						m_args.add(new CommandArgumentDescription(m.getName(), descriptionString, m.isAnnotationPresent(Required.class), m.isAnnotationPresent(StoreValue.class), options));
					} else if(m.isAnnotationPresent(DynamicSelect.class)) {
						m_args.add(new CommandArgumentDescription(m.getName(), descriptionString, m.isAnnotationPresent(Required.class), m.isAnnotationPresent(StoreValue.class), m.getAnnotation(DynamicSelect.class).value()));
					} else if(m.isAnnotationPresent(RunConfig.class)) {
						m_args.add(new CommandArgumentDescription(m.getName(), descriptionString, m.isAnnotationPresent(Required.class), m.isAnnotationPresent(StoreValue.class), RunConfig.class.getName()));
					} else if(m.isAnnotationPresent(WorkspaceBundleSelect.class)) {
						m_args.add(new CommandArgumentDescription(m.getName(), descriptionString, m.isAnnotationPresent(Required.class), m.isAnnotationPresent(StoreValue.class), WorkspaceBundleSelect.class.getName()));
					} else if(m.isAnnotationPresent(ProjectBndFile.class)) {
						m_args.add(new CommandArgumentDescription(m.getName(), descriptionString, m.isAnnotationPresent(Required.class), m.isAnnotationPresent(StoreValue.class), ProjectBndFile.class.getName()));
					}
					else {
						m_args.add(new CommandArgumentDescription(m.getName(), m.getReturnType().getName(), descriptionString, m.isAnnotationPresent(Required.class), m.isAnnotationPresent(StoreValue.class)));
					}
					
				});
			} else if(arg.getType() != Prompt.class) {
				m_args.add(new CommandArgumentDescription(arg.getName(), arg.getType().getName(), null, arg.isAnnotationPresent(Required.class), arg.isAnnotationPresent(Required.class)));
			}
		}
		
		private EnumValue createEnumValue(Object o) {
			if(DescribedEnum.class.isAssignableFrom(o.getClass())) {
				return new EnumValue(o.toString(), ((DescribedEnum) o).getDescription());
			} else {
				return new EnumValue(o.toString(), o.toString());
			}
		}
		
		public PluginDescriptionBuilder plugin() {
			return m_pluginDescriptionBuilder;
		}
		
		public CommandDescription build() {
			return new CommandDescription(m_name, m_description, m_scope, m_args);
		}
	}
	
}
