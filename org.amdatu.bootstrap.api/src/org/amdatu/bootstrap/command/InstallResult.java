/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.command;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;

import org.amdatu.bootstrap.services.Dependency;

import aQute.bnd.annotation.ProviderType;

/**
 * Lists the dependencies in a build or run configuration that were added/updated or skipped. 
 * Each {@link Command} that installs dependencies should return this type. 
 */
@ProviderType
public class InstallResult {
	private final List<Dependency> m_installedDependencies;
	private final List<Dependency> m_skippedDependencies;
	private final List<Dependency> m_updatedDependencies;
	
	public InstallResult(List<Dependency> installedDependencies, List<Dependency> skippedDependencies, List<Dependency> updatedDependencies) {
		m_installedDependencies = installedDependencies;
		m_skippedDependencies = skippedDependencies;
		m_updatedDependencies = updatedDependencies;
	}

	public List<Dependency> getInstalledDependencies() {
		return m_installedDependencies;
	}

	public List<Dependency> getSkippedDependencies() {
		return m_skippedDependencies;
	}
	
	public List<Dependency> getUpdatedDependencies() {
		return m_updatedDependencies;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("### Installed dependencies ###\n");
		
		Consumer<? super Dependency> append = d -> sb.append(d.getBsn()).append(" ").append(d.getVersion()).append("\n");
		m_installedDependencies.forEach(append);
		
		sb.append("\n\n").append("### Updated dependencies ###\n");
		m_updatedDependencies.forEach(append);
		
		sb.append("\n\n").append("### Skipped dependencies ###\n");
		m_skippedDependencies.forEach(append);
		
		return sb.toString();
	}

	public static Builder builder() {
		return new Builder();
	}

	public static class Builder {
		private List<Dependency> m_installedDependencies = new ArrayList<>();
		private List<Dependency> m_skippedDependencies = new ArrayList<>();
		private List<Dependency> m_updatedDependencies = new ArrayList<>();
		
		public Builder addInstalled(Dependency dependency) {
			m_installedDependencies.add(dependency);
			return this;
		}
		
		public Builder addSkipped(Dependency dependency) {
			m_skippedDependencies.add(dependency);
			return this;
		}
		
		public Builder addUpdated(Dependency dependency) {
			m_updatedDependencies.add(dependency);
			return this;
		}
		
		public Builder addResults(Collection<InstallResult> results) {
			results.forEach(this::addResult);
			return this;
		}
		
		public Builder addResult(InstallResult result) {
			if(result == null) {
				return this;
			}
			
			if(result.getInstalledDependencies() != null) {
				result.getInstalledDependencies().forEach(m_installedDependencies::add);
			}
			
			if(result.getSkippedDependencies() != null) {
				result.getSkippedDependencies().forEach(m_skippedDependencies::add);
			}
			
			if(result.getUpdatedDependencies() != null) { 
				result.getUpdatedDependencies().forEach(m_updatedDependencies::add);
			}
			
			return this;
		}
		
		public InstallResult build() {
			return new InstallResult(m_installedDependencies, m_skippedDependencies, m_updatedDependencies);
		}
	}
	
	@SuppressWarnings("unchecked")
	public static InstallResult empty() {
		return new InstallResult(Collections.EMPTY_LIST, Collections.EMPTY_LIST, Collections.EMPTY_LIST);
	}
}
